recognize:
	lea 	di, HEX_OUT
	cmp 	byte ptr [di], '0'	;0000
	je 		cmd_0
	cmp 	byte ptr [di], '1'	;0001
	je 		cmd_1
	cmp 	byte ptr [di], '2'	;0010
	je 		cmd_2
	cmp 	byte ptr [di], '3'	;0011
	je 		cmd_3
	cmp 	byte ptr [di], '4'	;0100
	je 		cmd_4
	cmp 	byte ptr [di], '5'	;0101
	je 		cmd_5
	cmp 	byte ptr [di], '6'	;0110
	je 		cmd_6
	cmp 	byte ptr [di], '7'	;0111
	je 		cmd_7
	cmp 	byte ptr [di], '8'	;1000
	je 		cmd_8
	cmp 	byte ptr [di], '9'	;1001
	je 		cmd_9
	cmp 	byte ptr [di], 'a'	;1010
	je 		cmd_a
	cmp 	byte ptr [di], 'b'	;1011
	je 		cmd_b
	cmp 	byte ptr [di], 'c'	;1100
	je 		cmd_c
	cmp 	byte ptr [di], 'd'	;1101
	je 		cmd_d
	cmp 	byte ptr [di], 'e'	;1110
	je 		cmd_e
	cmp 	byte ptr [di], 'f'	;1111
	je 		cmd_f
	jmp 	closing

cmd_0:

cmd_1:

cmd_2:

cmd_3:

cmd_4:

cmd_5:

cmd_6:

cmd_7:

cmd_8:

cmd_9:

cmd_a:

cmd_b:

cmd_c:

cmd_d:

cmd_e:

cmd_f:


;senas

;komandu atpazinimas
;iskvietus komandos procedura, ji pati turi is bufferio paimt naujus baitus ir atpazint parametrus
;kai baigiame spausdint procedura griztame i readinga, kur spausdinsime nauja eilute ir viska kartosime
recognize proc near
	cmp 	byte ptr [di], '0'	;0000
	je 		cmd_0
	cmp 	byte ptr [di], '1'	;0001
	je 		cmd_1
	cmp 	byte ptr [di], '2'	;0010
	je 		cmd_2
	cmp 	byte ptr [di], '3'	;0011
	je 		cmd_3
	cmp 	byte ptr [di], '4'	;0100
	je 		cmd_4
	cmp 	byte ptr [di], '5'	;0101
	je 		cmd_5
	cmp 	byte ptr [di], '6'	;0110
	je 		cmd_6
	cmp 	byte ptr [di], '7'	;0111
	je 		cmd_7
	cmp 	byte ptr [di], '8'	;1000
	je 		cmd_8
	cmp 	byte ptr [di], '9'	;1001
	je 		cmd_9
	cmp 	byte ptr [di], 'a'	;1010
	je 		cmd_a
	cmp 	byte ptr [di], 'b'	;1011
	je 		cmd_b
	cmp 	byte ptr [di], 'c'	;1100
	je 		cmd_c
	cmp 	byte ptr [di], 'd'	;1101
	je 		cmd_d
	cmp 	byte ptr [di], 'e'	;1110
	je 		cmd_e
	cmp 	byte ptr [di], 'f'	;1111
	je 		cmd_f
	jmp 	closing

cmd_0:

cmd_1:

cmd_2:

cmd_3:

cmd_4:
	mov 	ah, [whichByte]
	cmp 	ah, 1
	je 	 	setCommandIn4
	cmp 	ah, 2
	je 		checkCommandIn4
	cmp 	ah, 3
	je 		notRecognized 	;laikinai
	ret
setCommandIn4:
	mov 	ah, 4
	mov 	[selectedCommand], ah
	;...
	ret
checkCommandIn4:
	mov 	ah, 4
 	mov 	[selectedByte], ah
 	mov 	ah, [selectedCommand]
 	cmp 	ah, 'b'
 	je 		callReg 	;b komandoj pirmas parametras tik registras
 callReg:
 	call 	print_register
 	ret

cmd_5:

cmd_6:

cmd_7:

cmd_8:

cmd_9:

cmd_a: 	;mov, movsbm cmpsb, stosb, lodsb, scasb

cmd_b:	;mov
	mov 	ah, [whichByte]
	cmp 	ah, 1
	je 		setCommandInb
	cmp 	ah, 2
	je 		notRecognized
	ret
setCommandInb:
	mov 	[selectedCommand], 'b'
	call 	print_mov
	ret
cmd_c:

cmd_d:

cmd_e:

cmd_f:

notRecognized:
	lea 	dx, recognize_err
	mov 	ah, 09h
	int 	21h
	ret

recognize endp

;komandu spausdinimo proceduros
print_mov proc near
	lea 	dx, command_mov 
	mov 	ah, 09h
	int 	21h
	lea 	dx, space
	int 	21h
	
	;padidinam vienu kad skaityti antra baita
	mov 	ah, [whichByte]
	inc 	ah
	mov 	[whichByte], ah
	inc 	di
	call 	recognize
	ret

print_mov endp
;registru spausdinimas, ziurima i pasirinkta baita
print_register proc near
	mov 	ah, [selectedByte]
	cmp 	ah, '0'
	je 		printAL
	cmp 	ah, '1'
	je 	 	printCL
	cmp 	ah, '2'
	je 	 	printDL
	cmp 	ah, '3'
	je 	 	printBL
	cmp 	ah, '4'
	je 		printAH
	cmp 	ah, '5'
	je 		printCH
	cmp 	ah, '6'
	je 		printDH
	cmp 	ah, '7'
	je 		printBH
printAL:
printCL:
printDL:
printBL:
printAH:
	lea 	dx, reg_ah
	mov 	ah, 09h
	int 	21h
printCH:
printDH:
printBH:
	ret	
print_register endp