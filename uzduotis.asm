.model small
.stack 100h
.data

	pertraukimas db " Zingsninio rezimo pertraukimas! $",13,10,"$"
	Enteris db 13, 10, "$"
	PranNe	db "Komanda ne INC", 13, 10, "$"
	
	
	FE db "FE $"
	FF db "FF $"
	
	
	kintax dw ?
	kintcx dw ?
	kintdx dw ?
	kintbx dw ?
	kintsp dw ?
	kintbp dw ?
	kintsi dw ?
	kintdi dw ?
	

	ie00 db " 00 $"
	ie01 db " 01 $"
	ie02 db " 02 $"
	ie03 db " 03 $"
	ie04 db " 04 $"
	ie05 db " 05 $"
	ie06 db " 06 $"
	ie07 db " 07 $"
	ie40 db " 40 $"
	ie41 db " 41 $"
	ie42 db " 42 $"
	ie43 db " 43 $"
	ie44 db " 44 $"
	ie45 db " 45 $"
	ie46 db " 46 $"
	ie47 db " 47 $"
	ie80 db " 80 $"
	ie81 db " 81 $"
	ie82 db " 82 $"
	ie83 db " 83 $"
	ie84 db " 84 $"
	ie85 db " 85 $"
	ie86 db " 86 $"
	ie87 db " 87 $"
	ieC0 db " C0 $"
	ieC1 db " C1 $"
	ieC2 db " C2 $"
	ieC3 db " C3 $"
	ieC4 db " C4 $"
	ieC5 db " C5 $"
	ieC6 db " C6 $"
	ieC7 db " C7 $"
	
	
	iebxsi db "bx + si $"
	iebxdi db "bx + di $"
	iebpsi db "bp + si $"
	iebpdi db "bp + di $"
	iesi   db "si $"
	iedi   db "di $"
;	ietsg  db "bx + si $"
	iebx   db "bx $"
	
	
	iebxsip db "bx + si + p $"
	iebxdip db "bx + di + p $"
	iebpsip db "bp + si + p $"
	iebpdip db "bp + di + p $"
	iesip   db "si + p $"
	iedip   db "di + p $"
	iebpp   db "bp + p $"
	iebxp   db "bx + p $"
	
	
	iebxsipp db "bx + si + pp $"
	iebxdipp db "bx + di + pp $"
	iebpsipp db "bp + si + pp $"
	iebpdipp db "bp + di + pp $"
	iesipp   db "si + pp $"
	iedipp   db "di + pp $"
	iebppp   db "bp + pp $"
	iebxpp   db "bx + pp $"
	
	
	ieal db "al $"
	iecl db "cl $"
	iedl db "dl $"
	iebl db "bl $"
	ieah db "ah $"
	iech db "ch $"
	iedh db "dh $"
	iebh db "bh $"
	
	ieall db "al= $"
	iecll db "cl= $"
	iedll db "dl= $"
	iebll db "bl= $"
	ieahl db "ah= $"
	iechl db "ch= $"
	iedhl db "dh= $"
	iebhl db "bh= $"
	ieaxl db "ax= $"
	iecxl db "cx= $"
	iedxl db "dx= $"
	iebxl db "bx= $"
	iespl db "sp= $"
	iebpl db "bp= $"
	iesil db "si= $"
	iedil db "di= $"
	
	
.code
Pradzia:
	MOV	ax, @data
	MOV	ds, ax

	MOV	ax, 0	
	MOV	es, ax		;į es įsirašome 0, nes pertraukimų vektorių lentelė yra segmente, kurio pradžios adresas yra 00000

	PUSH	es:[4]   ;Iššisaugome tikrą pertraukimo apdorojimo procedūros adresą, kad programos gale galėtume jį atstatyti
	PUSH	es:[6]
	
;Pertraukimų vektorių lentelėje suformuojame pertraukimo apdorojimo procedūros adresą
	MOV	word ptr es:[4], offset ApdorokPertr	;į pertraukimų vektorių lentelę įrašome pertraukimo apdorojimo procedūros poslinkį nuo kodo segmento pradžios
	MOV	es:[6], cs				;į pertraukimų vektorių lentelę įrašome pertraukimo apdorojimo procedūros segmentą


	;Testuojame pertraukimo apdorojimo procedūrą
	PUSHF			;Išsisaugome SF reikšmę testavimo pradžioje
	PUSHF			;Išsisaugome SF kad galėtume ją išimti ir nustatyti TF
	POP ax			;Išimame SF reikšmę į TF
	OR ax, 0100h		;Nustatome TF=1
	PUSH ax			;Įdedame pakoreguotą reikšmę
	POPF			;Išimame pakoreguotą reikšmę į SF; Nuo čia TF=1
	NOP			;Pirmas pertraukimas kyla ne prieš šią komandą, o po jos; todėl tiesiog vieną komandą nieko nedarome

;Šitas komandas nagrinės pertraukimo apdorojimo procedūra

	inc word ptr[bx+si]
	
	POPF			;Ištraukiame iš steko testavimo pradžioje buvusią SF reikšmę
				;Kadangi tada TF buvo lygi 0, tai tokiu būdu numušame TF
				
;Atstatome tikrą pertraukimo apdorojimo programos adresą pertraukimų vektoriuje
	POP	es:[6]
	POP	es:[4]

	MOV	ax, 4C00h			
	INT	21h		

;****************************************************************************
;Pertraukimo apdorojimo procedūra

PROC ApdorokPertr
	;Įdedame registrų reikšmes į steką
	PUSH	ax
	PUSH	bx
	PUSH	dx
	PUSH	bp
	PUSH	es
	PUSH	ds
	
	mov kintax,ax
	mov kintcx,cx
	mov kintdx,dx
	mov kintbx,bx
	mov kintsp,sp
	mov kintbp,bp
	mov kintsi,si
	mov kintdi,di
	

	MOV	ax, @data      ;Nustatome DS reikšmę, jei pertraukimą iškviestų kita programa
	MOV	ds, ax

	;Į registrą DL įsirašom komandos, prieš kurią buvo iškviestas INT, operacijos kodą
	MOV bp, sp		;Darbui su steku patogiausia naudoti registra BP
	ADD bp, 12		;Suskaičiuojame kaip giliai steke yra įdėtas grįžimo adresas
	MOV bx, [bp]		;Į bx įdedame grįžimo adreso poslinkį nuo segmento pradžios (IP)
	MOV es, [bp+2]		;Į es įdedame grįžimo adreso segmentą (CS)
	MOV dl, [es:bx]		;Išimame pirmąjį baitą, esantį grįžimo adresu - komandos OPK

	;Tikriname, ar INT buvo iškviestas prieš komandą inc
	MOV al, dl
	CMP al, 0FFh		;Ar tai inc - 1111 1111
	JE  incFF
	
	MOV al, dl
	CMP al, 0FEh		;Ar tai inc - 1111 1110
	JE incFE
	
	MOV al, dl
	and al, 11110000b
	CMP al, 01000000b	;Ar tai inc - ax - di
	JE increg
	
	;Jei INT buvo iškviestas ne prieš komandą MOV, tai išvedame pranešimą
	MOV ah, 9
	MOV dx, offset PranNe
	INT 21h
	JMP pabaiga

;********************************************************
	;Jei INT buvo iškviestas prieš komandą MOV, tai tada dl registre suformuojame bito w reikšmę
IncFF:
	call zingsninis
	call adresas
	MOV ah, 9
	MOV dx, offset FF
	INT 21h
	JMP incFFtoliau

;***********************************************************
	
IncFE:
	call zingsninis
	call adresas
	MOV ah, 9
	MOV dx, offset FE
	INT 21h
	jmp incFEtoliau
	
Increg:
	jmp incregtoliau

;************************************************
	
IncFFtoliau:	
	mov dl, [es:bx+1]
	push dx                         ;gali neveikt
	and dl, 11110000b 
	cmp dl, 00000000b
	jne toliau1
	jmp vedam0x
toliau1:
	cmp dl, 01000000b
	jne toliau2
	jmp vedam4x
toliau2:
	cmp dl, 10000000b
	jne toliau3
	jmp vedam8x
toliau3: 
	jmp sok
	
;**************************************************

IncFEtoliau:
	mov dl, [es:bx+1]
	push dx
	and dl, 11110000b
	cmp dl, 11000000b
	jne toliau4
	jmp vedamCx
toliau4:
	jmp sok
	
Incregtoliau:
	cmp dl,01000000b
	je vvax
	cmp dl,01000001b
	je vvcx
	cmp dl,01000010b
	je vvdx
	cmp dl,01000011b
	je vvbx
	cmp dl,01000100b
	je vvsp
	cmp dl,01000101b
	je vvbp
	cmp dl,01000110b             
	je vvsi
	cmp dl,01000111b
	je vvdi
	
vvax:
	MOV ah,09h
	MOV dx, offset ie40
	INT 21h
	jmp sok
vvcx:
	MOV ah,09h
	MOV dx, offset ie41
	INT 21h
	jmp sok
vvdx:
	MOV ah,09h
	MOV dx, offset ie42
	INT 21h
	jmp sok
vvbx:
	MOV ah,09h
	MOV dx, offset ie43
	INT 21h
	jmp sok
vvsp:
	MOV ah,09h
	MOV dx, offset ie44
	INT 21h
	jmp sok
vvbp:
	MOV ah,09h
	MOV dx, offset ie45
	INT 21h
	jmp sok
vvsi:
	MOV ah,09h
	MOV dx, offset ie46
	INT 21h
	jmp sok
vvdi:
	MOV ah,09h
	MOV dx, offset ie47
	INT 21h
	jmp sok

;*******************************************************	
vedam0x:
	pop dx
	cmp dl,00000000b
	je vbxsi
	cmp dl,00000001b
	je vbxdi
	cmp dl,00000010b
	jne tol3
	jmp vbpsi
tol3:
	cmp dl,00000011b
	jne tol4
	jmp vbpdi
tol4:
	cmp dl,00000100b
	jne tol5
	jmp vsi
tol5:
	cmp dl,00000101b
	jne tol6
	jmp vdi
tol6:
;	cmp dl,00000110b              ;reikia padirbeti
;	je vbxsi
	cmp dl,00000111b
	jmp vbx
	
vbxsi:
	MOV ah,09h
	MOV dx, offset ie00
	INT 21h
	MOV ah,09h
	MOV dx, offset iebxsi
	INT 21h
	mov ah,09h
	mov dx,offset iebxl
	int 21h
	mov dx,kintbx
	call skaiciai4
	mov ah,02h
	mov dl,20h
	int 21h
	mov ah,09h
	mov dx,offset iesil
	int 21h
	mov dx,kintsi
	call skaiciai4
	jmp sok
vbxdi:
	MOV ah,09h
	MOV dx, offset ie01
	INT 21h
	MOV ah,09h
	MOV dx, offset iebxdi
	INT 21h
	mov ah,09h
	mov dx,offset iebxl
	int 21h
	mov dx,kintbx
	call skaiciai4
	mov ah,02h
	mov dl,20h
	int 21h
	mov ah,09h
	mov dx,offset iedil
	int 21h
	mov dx,kintdi
	call skaiciai4
	jmp sok
vbpsi:
	MOV ah,09h
	MOV dx, offset ie02
	INT 21h
	MOV ah,09h
	MOV dx, offset iebpsi
	INT 21h
	mov ah,09h
	mov dx,offset iebpl
	int 21h
	mov dx,kintbp
	call skaiciai4
	mov ah,02h
	mov dl,20h
	int 21h
	mov ah,09h
	mov dx,offset iesil
	int 21h
	mov dx,kintsi
	call skaiciai4
	jmp sok
vbpdi:
	MOV ah,09h
	MOV dx, offset ie03
	INT 21h
	MOV ah,09h
	MOV dx, offset iebpdi
	INT 21h
	mov ah,09h
	mov dx,offset iebpl
	int 21h
	mov dx,kintbp
	call skaiciai4
	mov ah,02h
	mov dl,20h
	int 21h
	mov ah,09h
	mov dx,offset iedil
	int 21h
	mov dx,kintdi
	call skaiciai4
	jmp sok
vsi:
	MOV ah,09h
	MOV dx, offset ie04
	INT 21h
	MOV ah,09h
	MOV dx, offset iesi
	INT 21h
	mov ah,09h
	mov dx,offset iesil
	int 21h
	mov dx,kintsi
	call skaiciai4
	jmp sok
vdi:
	MOV ah,09h
 	MOV dx, offset ie05
	INT 21h
	MOV ah,09h
	MOV dx, offset iedi
	INT 21h
	mov ah,09h
	mov dx,offset iedil
	int 21h
	mov dx,kintdi
	call skaiciai4
	jmp sok
;vtsg:
;	MOV ah,09h
;	MOV dx, offset ie06
;	INT 21h
;	MOV ah,09h
;	MOV dx, offset ietsg
;	INT 21h
;	jmp sok
vbx:
	MOV ah,09h
	MOV dx, offset ie07
	INT 21h
	MOV ah,09h
	MOV dx, offset iebx
	INT 21h
	mov ah,09h
	mov dx,offset iebxl
	int 21h
	mov dx,kintbx
	call skaiciai4
	jmp sok
	
;*******************************************************************	
vedam4x:
	
	pop dx
	cmp dl,01000000b
	je vbxsip
	cmp dl,01000001b
	je vbxdip
	cmp dl,01000010b
	jne tol33
	jmp vbpsip
tol33:
	cmp dl,01000011b
	jne tol44
	jmp vbpdip
tol44:
	cmp dl,01000100b
	jne tol55 
	jmp vsip
tol55:
	cmp dl,01000101b
	jne tol66
	jmp vdip
tol66:
	cmp dl,01000110b 
	jne tol77	
	jmp vbpp
tol77:
	cmp dl,01000111b
	jmp vbxp
	
vbxsip:
	MOV ah,09h
	MOV dx, offset ie40
	INT 21h
	mov dl,[es:bx+2]
	call skaiciai
	MOV ah,09h
	MOV dx, offset iebxsip
	INT 21h
	call posl1
	mov ah,09h
	mov dx,offset iebxl
	int 21h
	mov dx,kintbx
	call skaiciai4
	mov ah,02h
	mov dl,20h
	int 21h
	mov ah,09h
	mov dx,offset iesil
	int 21h
	mov dx,kintsi
	call skaiciai4
	jmp sok
vbxdip:
	MOV ah,09h
	MOV dx, offset ie41
	INT 21h
	mov dl,[es:bx+2]
	call skaiciai
	MOV ah,09h
	MOV dx, offset iebxdip
	INT 21h
	call posl1
	mov ah,09h
	mov dx,offset iebxl
	int 21h
	mov dx,kintbx
	call skaiciai4
	mov ah,02h
	mov dl,20h
	int 21h
	mov ah,09h
	mov dx,offset iedil
	int 21h
	mov dx,kintdi
	call skaiciai4
	jmp sok
vbpsip:
	MOV ah,09h
	MOV dx, offset ie42
	INT 21h
	mov dl,[es:bx+2]
	call skaiciai
	MOV ah,09h
	MOV dx, offset iebpsip
	INT 21h
	call posl1
	mov ah,09h
	mov dx,offset iebpl
	int 21h
	mov dx,kintbp
	call skaiciai4
	mov ah,02h
	mov dl,20h
	int 21h
	mov ah,09h
	mov dx,offset iesil
	int 21h
	mov dx,kintsi
	call skaiciai4
	jmp sok
vbpdip:
	MOV ah,09h
	MOV dx, offset ie43
	INT 21h
	mov dl,[es:bx+2]
	call skaiciai
	MOV ah,09h
	MOV dx, offset iebpdip
	INT 21h
	call posl1
	mov ah,09h
	mov dx,offset iebpl
	int 21h
	mov dx,kintbp
	call skaiciai4
	mov ah,02h
	mov dl,20h
	int 21h
	mov ah,09h
	mov dx,offset iedil
	int 21h
	mov dx,kintdi
	call skaiciai4
	jmp sok
vsip:
	MOV ah,09h
	MOV dx, offset ie44
	INT 21h
	mov dl,[es:bx+2]
	call skaiciai
	MOV ah,09h
	MOV dx, offset iesip
	INT 21h
	call posl1
	mov ah,09h
	mov dx,offset iesil
	int 21h
	mov dx,kintsi
	call skaiciai4
	jmp sok
vdip:
	MOV ah,09h
	MOV dx, offset ie45
	INT 21h
	mov dl,[es:bx+2]
	call skaiciai
	MOV ah,09h
	MOV dx, offset iedip
	INT 21h
	call posl1
	mov ah,09h
	mov dx,offset iedil
	int 21h
	mov dx,kintdi
	call skaiciai4
	jmp sok
vbpp:
	MOV ah,09h
	MOV dx, offset ie46
	INT 21h
	mov dl,[es:bx+2]
	call skaiciai
	MOV ah,09h
	MOV dx, offset iebpp
	INT 21h
	call posl1
	mov ah,09h
	mov dx,offset iebpl
	int 21h
	mov dx,kintbp
	call skaiciai4
	jmp sok
vbxp:
	MOV ah,09h
	MOV dx, offset ie47
	INT 21h
	mov dl,[es:bx+2]
	call skaiciai
	MOV ah,09h
	MOV dx, offset iebxp
	INT 21h
	call posl1
	mov ah,09h
	mov dx,offset iebxl
	int 21h
	mov dx,kintbx
	call skaiciai4
	jmp sok

;********************************************	

vedam8x:
	pop dx
	cmp dl,10000000b
	je vbxsipp
	cmp dl,10000001b
	je vbxdipp
	cmp dl,10000010b
	jne tol333
	jmp vbpsipp
tol333:
	cmp dl,10000011b
	jne tol444 
	jmp vbpdipp
tol444:
	cmp dl,10000100b
	jne tol555
	jmp vsipp
tol555:
	cmp dl,10000101b
	jne tol666
	jmp vdipp
tol666:
	cmp dl,10000110b
	jne tol777	
	jmp vbppp
tol777:
	cmp dl,10000111b
	jmp vbxpp
	
vbxsipp:
	MOV ah,09h
	MOV dx, offset ie80
	INT 21h
	mov dl,[es:bx+2]
	call skaiciai
	mov dl,[es:bx+3]
	call skaiciai
	MOV ah,09h
	MOV dx, offset iebxsipp
	INT 21h
	call posl2
	mov ah,09h
	mov dx,offset iebxl
	int 21h
	mov dx,kintbx
	call skaiciai4
	mov ah,02h
	mov dl,20h
	int 21h
	mov ah,09h
	mov dx,offset iesil
	int 21h
	mov dx,kintsi
	call skaiciai4
	jmp sok
vbxdipp:
	MOV ah,09h
	MOV dx, offset ie81
	INT 21h
	mov dl,[es:bx+2]
	call skaiciai
	mov dl,[es:bx+3]
	call skaiciai
	MOV ah,09h
	MOV dx, offset iebxdipp
	INT 21h
	call posl2
	mov ah,09h
	mov dx,offset iebxl
	int 21h
	mov dx,kintbx
	call skaiciai4
	mov ah,02h
	mov dl,20h
	int 21h
	mov ah,09h
	mov dx,offset iedil
	int 21h
	mov dx,kintdi
	call skaiciai4
	jmp sok
vbpsipp:
	MOV ah,09h
	MOV dx, offset ie82
	INT 21h
	mov dl,[es:bx+2]
	call skaiciai
	mov dl,[es:bx+3]
	call skaiciai
	MOV ah,09h
	MOV dx, offset iebpsipp
	INT 21h
	call posl2
	mov ah,09h
	mov dx,offset iebpl
	int 21h
	mov dx,kintbp
	call skaiciai4
	mov ah,02h
	mov dl,20h
	int 21h
	mov ah,09h
	mov dx,offset iesil
	int 21h
	mov dx,kintsi
	call skaiciai4
	jmp sok
vbpdipp:
	MOV ah,09h
	MOV dx, offset ie83
	INT 21h
	mov dl,[es:bx+2]
	call skaiciai
	mov dl,[es:bx+3]
	call skaiciai
	MOV ah,09h
	MOV dx, offset iebpdipp
	INT 21h
	call posl2
	mov ah,09h
	mov dx,offset iebpl
	int 21h
	mov dx,kintbp
	call skaiciai4
	mov ah,02h
	mov dl,20h
	int 21h
	mov ah,09h
	mov dx,offset iedil
	int 21h
	mov dx,kintdi
	call skaiciai4
	jmp sok
vsipp:
	MOV ah,09h
	MOV dx, offset ie84
	INT 21h
	mov dl,[es:bx+2]
	call skaiciai
	mov dl,[es:bx+3]
	call skaiciai
	MOV ah,09h
	MOV dx, offset iesipp
	INT 21h
	call posl2
	mov ah,09h
	mov dx,offset iesil
	int 21h
	mov dx,kintsi
	call skaiciai4
	jmp sok
vdipp:
	MOV ah,09h
	MOV dx, offset ie85
	INT 21h
	mov dl,[es:bx+2]
	call skaiciai
	mov dl,[es:bx+3]
	call skaiciai
	MOV ah,09h
	MOV dx, offset iedipp
	INT 21h
	call posl2
	mov ah,09h
	mov dx,offset iedil
	int 21h
	mov dx,kintdi
	call skaiciai4
	jmp sok
vbppp:
	MOV ah,09h
	MOV dx, offset ie86
	INT 21h
	mov dl,[es:bx+2]
	call skaiciai
	mov dl,[es:bx+3]
	call skaiciai
	MOV ah,09h
	MOV dx, offset iebppp
	INT 21h
	call posl2
	mov ah,09h
	mov dx,offset iebpl
	int 21h
	mov dx,kintbp
	call skaiciai4
	jmp sok
vbxpp:
	MOV ah,09h
	MOV dx, offset ie87
	INT 21h
	mov dl,[es:bx+2]
	call skaiciai
	mov dl,[es:bx+3]
	call skaiciai
	MOV ah,09h
	MOV dx, offset iebxpp
	INT 21h
	call posl2
	mov ah,09h
	mov dx,offset iebxl
	int 21h
	mov dx,kintbx
	call skaiciai4
	jmp sok
	
;********************************************************

vedamCx:
	pop dx
	cmp dl,11000000b
	je val
	cmp dl,11000001b
	je vcl
	cmp dl,11000010b
	jne toll3 
	jmp vdl
toll3:
	cmp dl,11000011b
	jne toll4 
	jmp vbl
toll4:
	cmp dl,11000100b
	jne toll5
	jmp vah
toll5:
	cmp dl,11000101b
	jne toll6
	jmp vch
toll6:
	cmp dl,11000110b            
	jne toll7
	jmp vdh
toll7:
	cmp dl,11000111b
	jmp vbh
	
val:
	MOV ah,09h
	MOV dx, offset ieC0
	INT 21h
	MOV ah,09h
	MOV dx, offset ieal
	INT 21h
	mov ah,09h
	mov dx,offset ieall
	int 21h
	mov dx,kintax
	mov dh,00h
	call skaiciai
	jmp sok
vcl:
	MOV ah,09h
	MOV dx, offset ieC1
	INT 21h
	MOV ah,09h
	MOV dx, offset iecl
	INT 21h
	mov ah,09h
	mov dx,offset iecll
	int 21h
	mov dx,kintcx
	mov dh,00h
	call skaiciai
	jmp sok
vdl:
	MOV ah,09h
	MOV dx, offset ieC2
	INT 21h
	MOV ah,09h
	MOV dx, offset iedl
	INT 21h
	mov ah,09h
	mov dx,offset iedll
	int 21h
	mov dx,kintdx
	mov dh,00h
	call skaiciai
	jmp sok
vbl:
	MOV ah,09h
	MOV dx, offset ieC3
	INT 21h
	MOV ah,09h
	MOV dx, offset iebl
	INT 21h
	mov ah,09h
	mov dx,offset iebll
	int 21h
	mov dx,kintbx
	mov dh,00h
	call skaiciai
	jmp sok
vah:
	MOV ah,09h
	MOV dx, offset ieC4
	INT 21h
	MOV ah,09h
	MOV dx, offset ieah
	INT 21h
	mov ah,09h
	mov dx,offset ieahl
	int 21h
	mov dx,kintax
	mov cl,dh
	mov dh,00h
	mov dl,cl
	call skaiciai
	jmp sok
vch:
	MOV dx, offset ieC5
	INT 21h
	MOV ah,09h
	MOV dx, offset iech
	INT 21h
	mov ah,09h
	mov dx,offset iechl
	int 21h
	mov dx,kintcx
	mov cl,dh
	mov dh,00h
	mov dl,cl
	call skaiciai
	jmp sok
vdh:
	MOV ah,09h
	MOV dx, offset ieC6
	INT 21h
	MOV ah,09h
	MOV dx, offset iedh
	INT 21h
	mov ah,09h
	mov dx,offset iedhl
	int 21h
	mov dx,kintdx
	mov cl,dh
	mov dh,00h
	mov dl,cl
	call skaiciai
	jmp sok
vbh:
	MOV ah,09h
	MOV dx, offset ieC7
	INT 21h
	MOV ah,09h
	MOV dx, offset iebh
	INT 21h
	mov ah,09h
	mov dx,offset iebhl
	int 21h
	mov dx,kintbx
	mov cl,dh
	mov dh,00h
	mov dl,cl
	call skaiciai
	jmp sok
	
	
sok:	
	MOV ah,09h
	MOV dx, offset enteris
	INT 21h

	;Atstatome registrų reikšmes ir išeiname iš pertraukimo apdorojimo procedūros
Pabaiga:
	POP ds
	POP es
	POP bp
	POP dx
	POP bx
	POP ax
	IRET			;pabaigoje būtina naudoti grįžimo iš pertraukimo apdorojimo procedūros komandą IRET
				;paprastas RET netinka, nes per mažai informacijos išima iš steko
ApdorokPertr ENDP
;********************************************
PROC zingsninis
	push ax
	push dx
	
	MOV ah, 9
	MOV dx, offset pertraukimas
	INT 21h
	
	pop dx
	pop ax
	ret
zingsninis endp
;********************************************
PROC adresas
	push ax
	push bx
	push dx
	
	mov dl,byte ptr[bp+3]
	call skaiciai
	mov dl,byte ptr[bp+2]
	call skaiciai
	mov ah,02h
	mov dl,3Ah
	int 21h
	mov dl,byte ptr[bp+1]
	call skaiciai
	mov dl,byte ptr[bp]
	call skaiciai
	
	pop dx
	pop bx
	pop ax
	ret
adresas endp
;********************************************
PROC posl1

	mov dl,[es:bx+2]
	call skaiciai
	
	ret
posl1 endp
;*******************************************
PROC posl2

	mov dl,[es:bx+3]
	call skaiciai

antrasposlinkis:	
	mov dl,[es:bx+2]
	call skaiciai
	
	ret
posl2 endp
;*******************************************	
PROC skaiciai
	push ax
        push bx
dalyba:
	mov ax,dx
	mov bh,00h
	mov bl,10h
	div bl
	mov dl,al
	push ax
	
	mov ax,dx
	mov bh,00h
	mov bl,10h
	div bl
	push ax
		
	pop ax
	cmp ah,09h
	ja addmore
	add ah,30h
	mov al,ah
	mov ah,02h
	mov dl,al
	int 21h
	jmp kitas
	
addmore:
	add ah,37h
	mov al,ah
	mov ah,02h
	mov dl,al
	int 21h
	
kitas:
	pop ax
	cmp ah,09h
	ja addmore2
	add ah,30h
	mov al,ah
	mov ah,02h
	mov dl,al
	int 21h
	jmp toliaupab
		
addmore2:
	add ah,37h
	mov al,ah
	mov ah,02h
	mov dl,al
	int 21h
	
toliaupab:
        pop bx
	pop ax
	
	RET
	
skaiciai ENDP
;********************************************
PROC skaiciai4
	push ax
        push bx
dalybaa:
	mov ax,dx
	mov ax,si
	mov bh,00h
	mov bl,ah
	mov ch,00h
	mov cl,al
	
	
	mov ax,cx
	mov dl,10h
	div dl
	mov cl,ah
	push cx
	mov cl,al
	push cx
	
	mov ax,bx
	mov dl,10h
	div dl
	mov bl,ah
	push bx
	mov bl,al
	push bx
	
	
	pop ax
	cmp ax,09h
	ja addmore1
	add ax,30h
	mov bx,ax
	mov ah,02h
	mov dx,bx
	int 21h
	jmp kitas1
	
addmore1:
	add ax,37h
	mov bx,ax
	mov ah,02h
	mov dx,ax
	int 21h
	
kitas1:
	pop ax
	cmp ax,09h
	ja addmore22
	add ax,30h
	mov bx,ax
	mov ah,02h
	mov dx,bx
	int 21h
	jmp kitas2
		
addmore22:
	add ax,37h
	mov bx,ax
	mov ah,02h
	mov dx,ax
	int 21h
kitas2:
	pop ax
	cmp ax,09h
	ja addmore3
	add ax,30h
	mov bx,ax
	mov ah,02h
	mov dx,bx
	int 21h
	jmp kitas3
		
addmore3:
	add ax,37h
	mov bx,ax
	mov ah,02h
	mov dx,ax
	int 21h
kitas3:
	pop ax
	cmp ax,09h
	ja addmore4
	add ax,30h
	mov bx,ax
	mov ah,02h
	mov dx,bx
	int 21h
	jmp toliaupabb
		
addmore4:
	add ax,37h
	mov bx,ax
	mov ah,02h
	mov dx,ax
	int 21h
	
toliaupabb:
    pop bx
	pop ax
	
	ret
	
skaiciai4 endp

END Pradzia