.model small
.stack 100h

.data
	request		db 'Iveskite teksto eilute su tarpais', 0Dh, 0Ah, '$'
	result		db 0Dh, 0Ah, 'Rezultatas: ', 0Dh, 0Ah, '$'
	char		db " "

	new_line 	db 	13,10,'$'
	space 		db 	" ",'$'

	buffer  	db 10 dup (?)

	screenBuff 	db ?

	sourceF 	db "simple.txt$"
	sourceFHandle	dw ?

	destF   	db 100 dup (0)
	destFHandle 	dw ?

.code

start:
	mov		ax, @data
	mov		ds, ax

source_from_file:
	lea		dx, sourceF	; failo pavadinimas
	mov		ah, 3dh                	; atidaro faila - komandos kodas
	mov		al, 0                  	; 0 - reading, 1-writing, 2-abu
	int		21h			; INT 21h / AH= 3Dh - open existing file
	mov		sourceFHandle, ax	; issaugojam filehandle
	xor 	ax, ax

read:
	mov		bx, sourceFHandle
	lea		dx, buffer       ; address of buffer in dx
	mov		cx, 10       		; kiek baitu nuskaitysim, po to sita naudoja LODSB
	mov		ah, 3fh         	; function 3Fh - read from file
	int		21h
	mov 	si, offset buffer 	;si po to keliauja i LODSB
	mov 	cx, ax
	cmp 	cx, 0
	je 		ending

write:
	LODSB 					
	mov 	dl, al
	mov 	ah, 09h
	int 	21h
	loop 	write
	jmp 	ending

ending:        
	mov		bx, sourceFHandle	; pabaiga skaitomo failo
	mov		ah, 3eh			; uzdaryti
	int		21h

	mov 	ah, 4ch 		; sustabdyti programa - http://www.computing.dcu.ie/~ray/teaching/CA296/notes/8086_bios_and_dos_interrupts.html#int21h_4Ch
	mov 	al, 0 		        ; be klaidu = 0
	int 	21h                     ; 21h -  dos pertraukimmas - http://www.computing.dcu.ie/~ray/teaching/CA296/notes/8086_bios_and_dos_interrupts.html#int21h
end start


