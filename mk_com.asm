.model small
	
.code
org    100H	
begin:
	jmp    main

main    proc    far        ; <=== Entry point (main function)
MOV [DI+2], al
MOV [BX+DI+2], al
MOV es:[DI+2], al
MOV [SI], al
MOV byte ptr [SI], 5
MOV DS:[bp+2], al
MOV al, [DI+2]
MOV bl, al
MOV bx, ax
MOV bx, 1
MOV dl, 1
MOV dx, @data

OUT 4, al
OUT 3, ax
OUT dx, al
OUT dx, ax

not al
not ax
not byte ptr es:[SI]
not byte ptr [SI]
not word ptr [SI]

RCR byte ptr [SI], 4
RCR word ptr [SI], 5
RCR al, 5
RCR ax, 5
RCR BYTE PTR [DI+4], cl
RCR cl, cl

xlat

mov 	dh, [bx+di]








	not 	bh
    mov    ax,4c00H
    int    21H
	rcr 	ch, 1
	
	mov 	ch, 09h
	out 	7, al
	int 	21H
	ret
	int 	21h
	mov 	dh, [bx+di]

main    endp                ;<=== End function
	
end begin                ;<=== End program


	;; tasm mk_com
	;; tlink /t mk_com
