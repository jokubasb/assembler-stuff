.model small
.stack 100h

.data

apie    	db 'Programa isvedanti faila sesioliktainemis reiksmemis',13,10,'$'
helptext 	db 'Skaitomo failo pavadinima parasykite kaip parametra. Pvz: 2uzd.exe simple.txt',13,10,'$'
err_s    	db 'Source failo nepavyko atidaryti skaitymui',13,10,'$'
filename	db 'simple.txt$'
HEX_OUT 	db '00',0,'$'
new_line 	db 	13,10,'$'
offsetNum 	db 	'00000000',0,'$'

buffer  	db 16 dup (?)

sourceF   	db 100 dup (0)
sourceFHandle	dw ?

.code

start:
	mov		ax, @data
	mov		es, ax			; es kad galetume naudot stosb funkcija: Store AL at address ES:(E)DI

	mov 	si, 81h 				;parametrai prasideda nuo 81

	call 	skip_spaces

	mov		al, byte ptr ds:[si]	; nuskaityti pirma parametro simboli
	cmp		al, 13			; jei nera parametru
	je		help			; tai isvesti pagalba

	mov		ax, word ptr ds:[si]
	cmp		ax, 3F2Fh        	; jei nuskaityta "/?" - 3F = '?'; 2F = '/'
	je		help                 	; rastas "/?", vadinasi reikia isvesti pagalba

	lea 	di, sourceF
	call 	read_filename
	cmp		byte ptr es:[sourceF], '$' ; jei nieko nenuskaite
	jne		cont
	jmp 	closing

help:
	mov		ax, @data
	mov		ds, ax

	mov		dx, offset helptext         
	mov		ah, 09h
	int		21h
jmphelp:
	jmp		closing

cont:
	mov		ax, @data
	mov		ds, ax

	mov 	ah, 09h
	lea 	dx, apie
	int 	21h

source_from_file:
	lea		dx, sourceF	; failo pavadinimas
	mov		ah, 3dh                	; atidaro faila - komandos kodas
	mov		al, 0                  	; 0 - reading, 1-writing, 2-abu
	int		21h			; INT 21h / AH= 3Dh - open existing file
	jc		file_error		; CF set on error AX = error code.
	mov		sourceFHandle, ax	; issaugojam filehandle
	xor 	ax, ax
	push 	ax

read:
	mov		bx, sourceFHandle
	lea		dx, buffer       ; address of buffer in dx
	mov		cx, 16         		; kiek baitu nuskaitysim, po to sita naudoja LODSB
	mov		ah, 3fh         	; function 3Fh - read from file
	int		21h

	mov 	si, offset buffer 	;si po to keliauja i LODSB
	jc 		jmphelp

	pop 	dx 				;skaiciuohame kiek is viso baitu nuskaiteme
	add 	dx, ax			;naudojamas offsetui
	push 	dx

	mov		cx, ax          	; bytes actually read
	push 	cx 				;pushinam kad pasiektu rawtext
	cmp		ax, 0			; jei nenuskaite
	jne 	printOffset
	jmp		ending			; tai ne pabaiga

printOffset:
	lea 	dx,	offsetNum
	mov 	ah, 09h
	int 	21h
line:
	LODSB 					
	mov 	dl, al
	push 	cx
	call 	convert
	pop 	cx

	lea 	dx, HEX_OUT 	;hex isvedimas
	mov 	ah, 09h
	int 	21h
	
	loop 	line 			;cx-- loopas baigiasi kai cx = 0

	pop 	cx
	call 	printRawText

	pop 	dx 				;visu nuskaitytu bitu countas
	push 	dx
	call 	getOffset

	lea 	dx, new_line 	;nauja eilute po 15 baitu
	mov 	ah, 09h
	int 	21h
	inc 	ah
	jmp 	read



file_error:
	mov		dx, offset err_s        
	mov		ah, 09h
	int		21h
	jmp 	closing

printRawText proc near
	mov 	si, offset buffer

	mov 	dl, '|'
	mov 	ah, 2
	int 	21h
printChar:
	LODSB 
	cmp 	al, 1fh
	jle 	notSymbol
	cmp 	al, 7dh
	jge 	notSymbol
	mov 	dl, al
printOne:
	mov 	ah, 2
	int 	21h
	loop 	printChar

	mov 	dl, '|'
	mov 	ah, 2
	int 	21h

	ret
notSymbol:
	mov 	dl, '.'
	jmp 	printOne
printRawText endp

skip_spaces PROC near

skip_spaces_loop:
	cmp 	byte ptr ds:[si], ' '
	jne 	skip_spaces_end
	inc 	si
	jmp 	skip_spaces_loop
skip_spaces_end:
	ret
skip_spaces ENDP

read_filename PROC near
	push	ax
	call	skip_spaces
read_filename_start:
	cmp		byte ptr ds:[si], 13	; jei nera parametru
	je		read_filename_end	; tai taip, tai baigtas failo vedimas
	cmp		byte ptr ds:[si], ' '	; jei tarpas
	jne		read_filename_next	; tai praleisti visus tarpus, ir sokti prie kito parametro
read_filename_end:
	mov		al, '$'			; irasyti '$' gale
	stosb                           ; Store AL at address ES:(E)DI, di = di + 1
	pop		ax
	ret
read_filename_next:
	lodsb				; uzkrauna kita simboli
	stosb                           ; Store AL at address ES:(E)DI, di = di + 1
	jmp 	read_filename_start

read_filename ENDP

convert proc near    
	mov 	cx, 2
charLoop:
	dec 	cx					
	mov 	al, dl
	shr 	dl, 4
	and 	al, 0Fh 			;gaunam 4 bitus ;0xF

	mov 	bx, offset HEX_OUT   ; paruosiam HEX_OUT
  	add 	bx, cx        ; pridedam cx, kad nustatyti kelintas baitas

	cmp 	al, 0Ah			;jei skaicius
	jl 		setHex
	add 	al, 27h			;jei raide
	jl 		setHex

setHex:
	add 	al, 30h
	mov 	byte [bx-1],al  ; pridedam al reiksme prie bx esancio string'o tam tikrose vietose
	cmp 	cx, 0
	je 		convertDone
	jmp 	charLoop

convertDone:
	ret
convert endp 

getOffset proc near    
	mov 	cx, 8
charLoop1:
	dec 	cx					
	mov 	ax, dx
	shr 	dx, 4
	and 	ax, 0Fh 			;gaunam 4 bitus ;0xF

	mov 	bx, offset offsetNum   ; paruosiam HEX_OUT
  	add 	bx, cx        ; pridedam cx, kad nustatyti kelintas baitas

	cmp 	ax, 0Ah			;jei skaicius
	jl 		setHex1
	add 	al, 27h			;jei raide
	jl 		setHex1

setHex1:
	add 	al, 30h
	mov 	byte [bx-1],al  ; pridedam al reiksme prie bx esancio string'o tam tikrose vietose
	cmp 	cx, 0
	je 		convertDone1
	jmp 	charLoop1

convertDone1:
	ret
getOffset endp 

ending:
	lea 	dx,	offsetNum
	mov 	ah, 09h
	int 	21h

closing:
	mov		bx, sourceFHandle	; pabaiga skaitomo failo
	mov		ah, 3eh			; uzdaryti
	int		21h

	mov 	ah, 4ch
	mov 	al, 0 		        ; be klaidu = 0
	int 	21h                     ; 21h -  dos pertraukimmas - http://www.computing.dcu.ie/~ray/teaching/CA296/notes/8086_bios_and_dos_interrupts.html#int21h
end start
