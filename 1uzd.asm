.model small
.stack 100h

MAX_SIZE = 250

.data
	request		db 'Iveskite teksto eilute su tarpais', 0Dh, 0Ah, '$'
	result		db 0Dh, 0Ah, 'Rezultatas: ', 0Dh, 0Ah, '$'
	char		db " "
	buffer		db MAX_SIZE, ?, MAX_SIZE dup (0)

.code

start:
	mov 	dx, @data            	; perkelti data i registra ax
	mov 	ds, dx               	; nustatyti ds rodyti i data segmenta

	mov		ah, 09h
	mov		dx, offset request
	int		21h

	mov		dx, offset buffer
	mov		ah, 0Ah
	int 	21h

	mov		cl, buffer[1]			;simboliu kiekis
	cmp		cl, 0
	je		ending

	mov		ah, 09h
	mov		dx, offset result
	int		21h

	mov		si, offset buffer +2
	xor		ch, ch
loopstart:
	LODSB

	cmp		al, " "					;jei tarpas zf flag
	je		space					;funkcija spausdint vietoj tarpo
	jmp		notspace

tesk:
	mov		ah, 2					;print char
	int 	21h

	loop	loopstart				;ziuri i cl registra
	jmp		ending
	
space:
	mov		dl, char				;dl registras islikes is buvusio ciklo
	jmp 	tesk

notspace:
	mov		char, al
	mov		dl, al				;dl registras islikes is buvusio ciklo
	jmp 	tesk

ending:        
	mov 	ah, 4ch 		; sustabdyti programa - http://www.computing.dcu.ie/~ray/teaching/CA296/notes/8086_bios_and_dos_interrupts.html#int21h_4Ch
	mov 	al, 0 		        ; be klaidu = 0
	int 	21h                     ; 21h -  dos pertraukimmas - http://www.computing.dcu.ie/~ray/teaching/CA296/notes/8086_bios_and_dos_interrupts.html#int21h
end start
