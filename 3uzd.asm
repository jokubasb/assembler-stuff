.model small
.stack 100h

jumps

.data
;esminiai
apie    	db 'DISSASMAS',13,10,'$'
helptext 	db 'Skaitomo failo ir rezultato pavadinima parasykite kaip parametra. Pvz: 2uzd.exe pvz.asm rez.txt',13,10,'$'
err_s    	db 'Source/destination failo nepavyko atidaryti skaitymui',13,10,'$'
filename	db 'simple.txt$'
recognize_err db 'NEATPAZINTA',13,10,'$'

;temp
teststr		db 'pavyko$',13,10,'$'
temp1 		db 0

;pildomi
selected_EA db 00000000,'$'
HEX_OUT1 	db '0','$'
HEX_OUT2 	db '0','$'
HEX_OUT3 	db '0','$'
HEX_OUT4 	db '0','$'
EA_size 	db 0
;counteriai
bytesLeft 	db 0

;pagalbiniai
new_line 	db 	13,10,'$'
space 		db 	" ",'$'
bracket_1 	db 	'[','$'
bracket_2 	db 	']','$'
_byteptr 	db 	'byte ptr $'

;bufferiai
buffer  	db 16 dup (?)
screenBuff 	db 30 dup (0)

;failams
sourceF   	db 100 dup (0)
sourceFHandle	dw ?
destF   	db 100 dup (0)
destFHandle 	dw ?

;statusai
whichByte 	db 1
selectedCommand db 0
selectedByte db 0
halfByte 	db 0
reg_recognize db 0

selected_register db 3 dup ('$')
testt db "b$"

;table
selected_W db 0
selected_reg db 0 ;0-7
selected_sr db 00
selected_s db 0
selected_v db 0
selected_d db 0
selected_mod db 0


stopstr db "$"

;efektyvus adresai

bxsi 	db 'BX+SI $'
bxdi 	db 'BX+DI $'
bpsi 	db 'BP+SI $'
bpdi 	db 'BP+DI $'
si_ 	db 'si $'
di_ 	db 'di $'
bx_ 	db 'bx $'



;komandu kodai
command_mov db 'MOV $'
command_int_3 db 'INT 3$'
command_int db 'INT $'
command_ret db 'RET $'
command_out db 'OUT $'
command_xlat db 'XLAT$'
command_not db 'NOT $'
command_rcr db 'RCR $'

;offsetui
how_many_read_bytes db '0000:     $'
read_bytes db '                $'
total_bytes_read dw 00


.code

start:
	mov		ax, @data
	mov		es, ax			; es kad galetume naudot stosb funkcija: Store AL at address ES:(E)DI

	mov 	si, 81h 				;parametrai prasideda nuo 81

	call 	skip_spaces

	mov		al, byte ptr ds:[si]	; nuskaityti pirma parametro simboli
	cmp		al, 13			; jei nera parametru
	je		help			; tai isvesti pagalba

	mov		ax, word ptr ds:[si]
	cmp		ax, 3F2Fh        	; jei nuskaityta "/?" - 3F = '?'; 2F = '/'
	je		help                 	; rastas "/?", vadinasi reikia isvesti pagalba

	lea 	di, sourceF
	call 	read_filename

	lea 	di, destF
	call 	read_filename

	cmp		byte ptr es:[sourceF], '$' ; jei nieko nenuskaite
	jne		cont
	jmp 	closing

help:
	mov		ax, @data
	mov		ds, ax

	mov		dx, offset helptext         
	mov		ah, 09h
	int		21h
jmphelp:
	jmp		closing

jmptoErr:
	jmp 	file_error

cont:
	mov		ax, @data
	mov		ds, ax

	mov 	ah, 09h
	lea 	dx, apie
	int 	21h

;sukuriame source faila
source_from_file:
	lea		dx, sourceF	; failo pavadinimas
	mov		ah, 3dh                	; atidaro faila - komandos kodas
	mov		al, 0                  	; 0 - reading, 1-writing, 2-abu
	int		21h			; INT 21h / AH= 3Dh - open existing file
	jc		jmptoErr		; CF set on error AX = error code.
	mov		sourceFHandle, ax	; issaugojam filehandle

;sukuriame destination faila
createDestFile:
	lea 	dx, destF
	mov 	cx, 0
	mov 	ah, 3Ch
	int  	21h
	jc 		jmptoErr
	mov 	destFHandle, ax
	xor 	ax, ax
	push 	ax
	jmp 	read

file_error:
	mov		dx, offset err_s        
	mov		ah, 09h
	int		21h
	jmp 	closing

;skaitome is source failo, irasome i bufferi ir priskiriame si kad veliau patogu butu imt is bufferio
read:
	mov		bx, sourceFHandle
	lea		dx, buffer       ; address of buffer in dx
	mov		cx, 16         		; kiek baitu nuskaitysim, po to sita naudoja LODSB
	mov		ah, 3fh         	; function 3Fh - read from file
	int		21h

	mov 	si, offset buffer 	;si po to keliauja i LODSB
	jc 		jmphelp

	mov		[bytesLeft], al          	; bytes actually read
	cmp		ax, 0			; jei nenuskaite
	je		closing			; tai ne pabaiga

	xor 	dx, dx
	xor 	cx, cx
dissasemble:
	mov 	cl, 1
	call 	recognize
	;jmp 	dissasemble
	;call 	notRecognized

notRecognized:
	call 	getOffset
	lea 	dx, how_many_read_bytes
	mov 	cx, 10
	mov 	ah, 40h
	int 	21h

	mov 	bx, destFHandle
	lea 	dx, recognize_err
	mov 	ah, 40h
	mov 	cx, 13
	int 	21h
	jmp 	dissasemble
;-------------------------------------------NELIECIAM-----------------------------------------------------------
;dl - tikrinamas baitas, dh - sekama kelintas baitas, cl - parametras, ch - kur medziui sustoti
recognize proc near
	

	cmp 	dh, 0
	je 		loadByte
	mov 	dl, al

	cmp 	dh, 3
	je 		loadByte
	jmp 	dontLoadByte

loadByte:
	push 	bx
	mov 	bx, [total_bytes_read]
	inc 	bx
	mov 	[total_bytes_read], bx
	pop 	bx
	push 	cx
	call 	getByte
	pop 	cx
	mov 	dl, al
	mov 	dh, 1

dontLoadByte:
	cmp 	dh, 2
	je 		dontShift
shift:
	shr 	dl, 4
dontShift:
	inc 	dh
	and 	dl, 00001111b 	;bet kokiu atveju paslepiam pirma pusbaiti nes skaitom tik antra
recognize_same_byte:
	cmp 	cl, 8
	je 		rec_cancel
	test 	dl, 1000b
	je 		got_0
	jmp 	got_1
;-------------------------------------------------------------------------------------------------------
got_1:
	cmp 	ch, 1 			;kai ch 1 tada medis sustoja cia ir ima parametra
	je 		parameter_1
	test 	dl, 0100b
	je 		got_10
	jmp 	got_11
parameter_1:
	cmp 	[selectedCommand], 'b'
	je 		set_W_1
	cmp 	[selectedCommand], '8'
	je 		addToReg_1
	ret
;-------------------------------------------------------------------------------------------------------
got_0:	
	cmp 	ch, 1
	je 		parameter_0
	test 	dl, 0100b
	je 		got_00
	jmp 	got_01
parameter_0:	
	cmp 	[selectedCommand], 'b'
	je 		set_W_0
	cmp 	[selectedCommand], '8'
	je 		addToReg_0
	ret
;-------------------------------------------------------------------------------------------------------
got_00:
	cmp 	ch, 2
	je 		parameter_00
	test 	dl, 0010b
	je 		got_000
	jmp 	got_001
parameter_00:
	cmp 	[selectedCommand], 'd'
	je 		construct_RCR
	mov 	bl, 0
	ret
;-------------------------------------------------------------------------------------------------------
got_01:
	cmp 	ch, 2
	je 		parameter_01
	test 	dl, 0010b
	je 		got_010
	jmp 	got_011
parameter_01:
	mov 	bl, 1
	ret
;-------------------------------------------------------------------------------------------------------
got_10:
	cmp 	ch, 2
	je 		parameter_10
	test 	dl, 0010b
	je 		got_100
	jmp 	got_101
parameter_10:
	cmp 	[selectedCommand], '8'
	je 		construct_MOV_3
	mov 	bl, 2
	ret
;-------------------------------------------------------------------------------------------------------
got_11:
	cmp 	ch, 2
	je 		parameter_11
	test 	dl, 0010b
	je 		got_110
	jmp 	got_111
parameter_11:
	mov 	bl, 3
	ret
;-------------------------------------------------------------------------------------------------------
got_000:
	test 	dl, 0001b
	je 		got_0000
	jmp 	got_0001
;-------------------------------------------------------------------------------------------------------
got_001:
	cmp 	ch, 3
	je 		parameter_001
	test 	dl, 0001b
	je 		got_0010
	jmp 	got_0011
parameter_001:
	cmp 	[selectedCommand], 'c'
	je 		construct_RET
;-------------------------------------------------------------------------------------------------------
got_010:
	test 	dl, 0001b
	je 		got_0100
	jmp 	got_0101
;-------------------------------------------------------------------------------------------------------
got_011:
	cmp 	ch, 3
	je 		parameter_011
	test 	dl, 0001b
	je 		got_0110
	jmp 	got_0111
parameter_011:
	cmp 	[selectedCommand], 'c'
	je 		construct_MOV_2
	cmp 	[selectedCommand], 'e'
	je		construct_OUT_Par
	cmp 	[selectedCommand], 'd'
	je 		print_XLAT
	cmp 	[selectedCommand], 'f'
	je 		construct_NOT
;-------------------------------------------------------------------------------------------------------
got_100:
	test 	dl, 0001b
	je 		got_1000
	jmp 	got_1001
;-------------------------------------------------------------------------------------------------------
got_101:
	test 	dl, 0001b
	je 		got_1010
	jmp 	got_1011
;-------------------------------------------------------------------------------------------------------
got_110:
	cmp 	ch, 3
	je 		parameter_110
	test 	dl, 0001b
	je 		got_1100
	jmp 	got_1101
parameter_110:
	cmp 	[selectedCommand], 'c'
	je 		construct_INT
;call_construct_INT:
;	call 	construct_INT
;-------------------------------------------------------------------------------------------------------
got_111:
	cmp 	ch, 3
	je 		parameter_111
	test 	dl, 0001b
	je 		got_1110
	jmp 	got_1111
parameter_111:
	cmp 	[selectedCommand], 'e'
	je		print_OUT
;-------------------------------------------------------------------------------------------------------
got_0000: 	;add, push, pop, or
	cmp 	cl, 3
	je 		getValue
	cmp 	cl, 2
	je 		parameter_0000
	cmp 	cl, 4
	je 		save_reg_000
	cmp 	cl, 5
	je 		save_mem_000
	ret
parameter_0000:
save_reg_000:
	mov 	bl, 000b
	jmp 	getRegister
save_mem_000:
	mov 	bl, 000b
	jmp 	geteffectiveAddress
	ret
;-------------------------------------------------------------------------------------------------------
got_0001: 	;adc, sbb
	cmp 	cl, 3
	je 		getValue
	cmp 	cl, 2
	je 		parameter_0001
	cmp 	cl, 4
	je 		save_reg_001
	cmp 	cl, 5
	je 		save_mem_001
	ret
parameter_0001:
save_reg_001:
	mov 	bl, 001b
	jmp 	getRegister
save_mem_001:
	mov 	bl, 001b
	jmp 	geteffectiveAddress
	ret
;-------------------------------------------------------------------------------------------------------
got_0010: 	;and, daa, sub, das
	cmp 	cl, 3
	je 		getValue
	cmp 	cl, 2
	je 		parameter_0010
	cmp 	cl, 4
	je 		save_reg_010
	cmp 	cl, 5
	je 		save_mem_010
	ret
parameter_0010:
save_reg_010:
	mov 	bl, 010b
	jmp 	getRegister
save_mem_010:
	mov 	bl, 010b
	jmp 	geteffectiveAddress
	ret
;-------------------------------------------------------------------------------------------------------
got_0011: 	;xor, aaa, cmp, aas
	cmp 	cl, 3
	je 		getValue
	cmp 	cl, 2
	je 		parameter_0011
	cmp 	cl, 4
	je 		save_reg_011
	cmp 	cl, 5
	je 		save_mem_011
	ret
parameter_0011:
save_reg_011:
	mov 	bl, 011b
	jmp 	getRegister
save_mem_011:
	mov 	bl, 011b
	jmp 	geteffectiveAddress
	ret
;-------------------------------------------------------------------------------------------------------
got_0100: 	;inc, dec
	cmp 	cl, 3
	je 		getValue
	cmp 	cl, 2
	je 		parameter_0100
	cmp 	cl, 4
	je 		save_reg_100
	cmp 	cl, 5
	je 		save_mem_100
	ret
parameter_0100:
save_reg_100:
	mov 	bl, 100b
	jmp 	getRegister
save_mem_100:
	mov 	bl, 100b
	jmp 	geteffectiveAddress
	ret
;-------------------------------------------------------------------------------------------------------
got_0101: 	;push, pop
	cmp 	cl, 3
	je 		getValue
	cmp 	cl, 2
	je 		parameter_0101
	cmp 	cl, 4
	je 		save_reg_101
	cmp 	cl, 5
	je 		save_mem_101
	ret
parameter_0101:
save_reg_101:
	mov 	bl, 101b
	jmp 	getRegister
save_mem_101:
	mov 	bl, 101b
	jmp 	geteffectiveAddress
	ret
;-------------------------------------------------------------------------------------------------------
got_0110: 	;
	cmp 	cl, 3
	je 		getValue
	cmp 	cl, 2
	je 		parameter_0110
	cmp 	cl, 4
	je 		save_reg_110
	cmp 	cl, 5
	je 		save_mem_110
	ret
parameter_0110:
save_reg_110:
	mov 	bl, 110b
	jmp 	getRegister
save_mem_110:
	mov 	bl, 110b
	jmp 	geteffectiveAddress
	ret
;-------------------------------------------------------------------------------------------------------
got_0111: 	;jo, jno, jnae, jae, je, jne, jbe, ja, js ,jns, jp, jnp, jl, jge, jle, jg
	cmp 	cl, 3
	je 		getValue
	cmp 	cl, 2
	je 		parameter_0100
	cmp 	cl, 4
	je 		save_reg_111
	cmp 	cl, 5
	je 		save_mem_111
	ret
parameter_0111:
save_reg_111:
	mov 	bl, 111b
	jmp 	getRegister
save_mem_111:
	mov 	bl, 111b
	jmp 	geteffectiveAddress
	ret
;-------------------------------------------------------------------------------------------------------
got_1000: 	;add, orr, adc, sbb, and, sub, xor, cmp, test, xchg, mov, lea, pop
	cmp 	cl, 3
	je 		getValue
	cmp 	cl, 1
	je 		analyze_1000
	ret
;-------------------------------------------------------------------------------------------------------
got_1001: 	
	cmp 	cl, 3
	je 		getValue
	ret
;-------------------------------------------------------------------------------------------------------
got_1010:
	cmp 	cl, 3
	je 		getValue
	ret
;-------------------------------------------------------------------------------------------------------
got_1011:
	cmp 	cl, 3
	je 		getValue
	cmp 	cl, 1
	je 		construct_MOV_1
	ret
;-------------------------------------------------------------------------------------------------------
got_1100:
	cmp 	cl, 3
	je 		getValue
	cmp 	cl, 1
	je 		analyze_1100
	cmp 	cl, 2
	je 		parameter_1100
	ret
parameter_1100:
	;cmp 	[selectedCommand], 'c'
	ret


;-------------------------------------------------------------------------------------------------------
got_1101:
	cmp 	cl, 3
	je 		getValue
	cmp 	cl, 2
	je 		parameter_1101
	cmp 	cl, 1
	je 		analyze_1101
	ret
parameter_1101:
	;cmp 	[selectedCommand], 'c'
	;je 		call_print_INT
	;ret
call_print_INT:
	;call 	print_INT
	ret
;-------------------------------------------------------------------------------------------------------
got_1110:
	cmp 	cl, 3
	je 		getValue
	cmp 	cl, 1
	je 		analyze_1110
	ret
;-------------------------------------------------------------------------------------------------------
got_1111:
	cmp 	cl, 3
	je 		getValue
	cmp 	cl, 1
	je 		analyze_1111
	
	ret
;-------------------------------------------------------------------------------------------------------

getLastBit:
	test 	dl, 0001b
	je 		gotLast_0
	jmp 	gotLast_1
gotLast_0:
	mov 	bl, 0
	ret
gotLast_1:
	mov 	bl, 1
	ret

getThirdBit:
	test 	dl, 0010b
	je 		gotThird_0
	jmp 	gotThird_1
gotThird_0:
	mov 	bl, 0
	ret
gotThird_1:
	mov 	bl, 1
	ret

getRegAfterMod:
	mov 	bl, dl
	and 	bl, 0011b
	shl 	bl, 1
	mov 	ch, 1
	mov 	[temp1], bl
	call 	recognize
	ret

addToReg_1:
	mov 	bl, [temp1]
	add 	bl, 1
	ret
addToReg_0:
	mov 	bl, [temp1]
	ret

set_W_1:
	mov 	[selected_W], '1'
	ret
	 
set_W_0:
	mov 	[selected_W], '0'
	ret

getValue:
	push 	ax
	push 	bx
	push 	cx
	push 	dx
	call 	convert
	pop 	dx
	pop 	cx
	pop 	bx
	pop 	ax
	ret

geteffectiveAddress:
	push 	ax
	push 	bx
	push 	cx
	push 	dx
	call 	getEA
	pop 	dx
	pop 	cx
	pop 	bx
	pop 	ax
rec_cancel:
	ret

recognize endp
;----------------------------------------------------
analyze_1111:
	mov 	[selectedCommand], 'f'
	mov 	ch, 3
	call 	recognize
	mov 	ch, 4
	xor 	cx, cx
	mov 	cl, 1
	mov 	[selectedCommand], 0
	mov 	[selected_W], 0
	mov 	[selected_d], 0
	mov 	[selected_mod], 0
	jmp 	dissasemble
analyze_1000:
	mov 	[selectedCommand], '8'
	mov 	ch, 2
	call 	recognize
	mov 	ch, 4
	xor 	cx, cx
	mov 	cl, 1
	mov 	[selectedCommand], 0
	mov 	[selected_W], 0
	mov 	[selected_d], 0
	mov 	[selected_mod], 0
	jmp 	dissasemble

analyze_1101:
	mov 	[selectedCommand], 'd'
	mov 	ch, 2
	call 	recognize
	mov 	ch, 3
	call 	recognize
	mov 	ch, 4
	xor 	cx, cx
	mov 	cl, 1
	mov 	[selectedCommand], 0
	mov 	[selected_W], 0
	mov 	[selected_d], 0
	mov 	[selected_mod], 0
	jmp 	dissasemble


analyze_1110:
	mov 	[selectedCommand], 'e'
	mov 	ch, 3
	call 	recognize
	mov 	ch, 4
	xor 	cx, cx
	mov 	cl, 1
	mov 	[selectedCommand], 0
	mov 	[selected_W], 0
	mov 	[selected_d], 0
	mov 	[selected_mod], 0
	jmp 	dissasemble

analyze_1100:
	mov 	[selectedCommand], 'c'
	mov 	ch, 3
	call 	recognize
	mov 	ch, 4
	xor 	cx, cx
	mov 	cl, 1
	mov 	[selectedCommand], 0
	mov 	[selected_W], 0
	mov 	[selected_d], 0
	mov 	[selected_mod], 0
	jmp 	dissasemble
	;jmp 	notRecognized 	;TODO error
;----------------------------------------------
construct_RCR proc near
	;mov 	cl, 8
	;call 	recognize
	call 	getThirdBit
	cmp 	bl, 1
	je 		RCR_1
	jmp 	RCR_0
RCR_1:
	mov 	[HEX_OUT1], '1'
	jmp 	RCR_cont
RCR_0:
	mov 	[HEX_OUT1], '0'
RCR_cont:
	call 	getLastBit
	cmp 	bl, 1
	je 		RCR_word
	jmp 	RCR_byte
RCR_word:
	mov 	[selected_W], 1
	jmp 	cont_RCR
RCR_byte:
	mov 	[selected_W], 0
cont_RCR:
	mov 	ch, 2
	call 	recognize 	;gaunam mod
	cmp 	bl, 0
	je 		RCR_3_0
	cmp 	bl, 1
	je 		RCR_3_1
	cmp 	bl, 2
	je 		RCR_3_2
	cmp 	bl, 3
	je 		RCR_3_3
RCR_3_0:
	mov 	[selected_mod], 0
	jmp 	RCR_3_getReg
RCR_3_1:
	mov 	[selected_mod], 1
	jmp 	RCR_3_getReg
RCR_3_2:
	mov 	[selected_mod], 2
	jmp 	RCR_3_getReg
RCR_3_3:
	mov 	[selected_mod], 3
RCR_3_getReg:
	mov 	ch, 4
	call 	getRegAfterMod
	;cmp 	bl, 011b
	;jne 	notRecognized
cont1_RCR:
	mov 	ch, 4
	mov 	cl, 4 	;gaunam registra
	and 	dl, 0111b
	call 	recognize_same_byte
	;call 	got_addr ;gaunam poslinki
print_RCR:
	mov 	bx, destFHandle
	call 	getOffset
	lea 	dx, how_many_read_bytes
	mov 	cx, 10
	mov 	ah, 40h
	int 	21h

	lea 	dx, command_rcr
	mov 	ah, 40h
	mov 	cx, 4
	int 	21h

	lea 	dx, selected_register
	mov 	cx, 2
	mov 	ah, 40h
	int 	21h

	lea 	dx, space
	mov 	cx, 1
	mov 	ah, 40h
	int 	21h

	lea 	dx, HEX_OUT1
	mov 	cx, 1
	mov 	ah, 40h
	int 	21h

	mov 	cx, 2
	lea 	dx, new_line
	mov 	ah, 40h
	int     21h
	mov 	cl, 1
	mov 	[selectedCommand], 0
	mov 	[selected_W], 0
	mov 	[selected_d], 0
	mov 	[selected_mod], 0
	jmp 	dissasemble
construct_RCR endp
construct_NOT proc near
	call 	getLastBit
	cmp 	bl, 1
	je 		NOT_word
	jmp 	NOT_byte
NOT_word:
	mov 	[selected_W], 1
	jmp 	cont_NOT
NOT_byte:
	mov 	[selected_W], 0
cont_NOT:
	mov 	ch, 2
	call 	recognize 	;gaunam mod
	cmp 	bl, 0
	je 		NOT_3_0
	cmp 	bl, 1
	je 		NOT_3_1
	cmp 	bl, 2
	je 		NOT_3_2
	cmp 	bl, 3
	je 		NOT_3_3
NOT_3_0:
	mov 	[selected_mod], 0
	jmp 	NOT_3_getReg
NOT_3_1:
	mov 	[selected_mod], 1
	jmp 	NOT_3_getReg
NOT_3_2:
	mov 	[selected_mod], 2
	jmp 	NOT_3_getReg
NOT_3_3:
	mov 	[selected_mod], 3
NOT_3_getReg:
	mov 	ch, 4
	call 	getRegAfterMod
	cmp 	bl, 010b
	jne 	notRecognized
cont1_NOT:
	mov 	ch, 4
	mov 	cl, 4 	;gaunam registra
	and 	dl, 0111b
	call 	recognize_same_byte
	;call 	got_addr ;gaunam poslinki
print_NOT:
	mov 	bx, destFHandle
	call 	getOffset
	lea 	dx, how_many_read_bytes
	mov 	cx, 10
	mov 	ah, 40h
	int 	21h

	lea 	dx, command_not
	mov 	ah, 40h
	mov 	cx, 4
	int 	21h

	lea 	dx, selected_register
	mov 	cx, 2
	mov 	ah, 40h
	int 	21h

	lea 	dx, space
	mov 	cx, 1
	mov 	ah, 40h
	int 	21h

	mov 	cx, 2
	lea 	dx, new_line
	mov 	ah, 40h
	int     21h
	
	mov 	ch, 4
	xor 	cx, cx
	mov 	cl, 1
	mov 	[selectedCommand], 0
	mov 	[selected_W], 0
	mov 	[selected_d], 0
	mov 	[selected_mod], 0
	jmp 	dissasemble

construct_NOT endp
construct_MOV_3 proc near
	call 	getThirdBit 	;gaunam d
	cmp 	bl, 1
	je 		MOV_3_rm_r
	jmp 	MOV_3_r_rm
MOV_3_rm_r:
	mov 	[selected_d], 1
	jmp 	cont_MOV_3
MOV_3_r_rm:
	mov 	[selected_d], 0
cont_MOV_3:
	call 	getLastBit 		;gaunam w
	cmp 	bl, 1
	je 		MOV_3_word
	jmp 	MOV_3_byte
MOV_3_word:
	mov 	[selected_W], 1
	jmp 	cont1_MOV_3
MOV_3_byte:
	mov 	[selected_W], 0
cont1_MOV_3:
	mov 	ch, 2
	call 	recognize 	;gaunam mod
	cmp 	bl, 0
	je 		MOV_3_0
	cmp 	bl, 1
	je 		MOV_3_1
	cmp 	bl, 2
	je 		mov_3_2
	cmp 	bl, 3
	je 		mov_3_3
MOV_3_0:
	mov 	[selected_mod], 0
	jmp 	MOV_3_getReg
MOV_3_1:
	mov 	[selected_mod], 1
	jmp 	MOV_3_getReg
MOV_3_2:
	mov 	[selected_mod], 2
	jmp 	MOV_3_getReg
MOV_3_3:
	mov 	[selected_mod], 3
MOV_3_getReg:
	mov 	ch, 4
	call 	getRegAfterMod ;gauname reg is dvieju pusbaiciu (viskas ten auto) gaunam registra i bl
	call 	getRegister
MOV_3_getMem:
	mov 	ch, 4
	mov 	cl, 5
	and 	dl, 0111b
	call 	recognize_same_byte

print_MOV_3:
	mov 	bx, destFHandle
	call 	getOffset
	lea 	dx, how_many_read_bytes
	mov 	cx, 10
	mov 	ah, 40h
	int 	21h

	lea 	dx, command_mov
	mov 	ah, 40h
	mov 	cx, 4
	int 	21h
	cmp 	[selected_d], 0
	je 		print_MOV_3_rm
	jmp 	print_MOV_3_reg

print_MOV_3_reg:
	lea 	dx, selected_register
	mov 	cx, 2
	mov 	ah, 40h
	int 	21h
	cmp 	[selected_d], 0
	je 		print_MOV_3_enter
	jmp 	print_MOV_3_space

print_MOV_3_space:
	lea 	dx, space
	mov 	cx, 1
	mov 	ah, 40h
	int 	21h

	cmp 	[selected_d], 0
	je 		print_MOV_3_reg
	jmp 	print_MOV_3_rm

print_MOV_3_rm:
	lea 	dx, bracket_1
	mov 	cx, 1
	mov 	ah, 40h
	int 	21h

	lea 	dx, selected_EA
	mov 	cl, [EA_size]
	mov 	ah, 40h
	int 	21h

	lea 	dx, bracket_2
	mov 	cx, 1
	mov 	ah, 40h
	int 	21h
	cmp 	[selected_d], 0
	je 		print_MOV_3_space
	jmp 	print_MOV_3_enter

print_MOV_3_enter:
	mov 	cx, 2
	lea 	dx, new_line
	mov 	ah, 40h
	int     21h

	ret
construct_MOV_3 endp
;---------------------------------------------------
construct_XLAT proc near
print_XLAT:
	mov 	bx, destFHandle
	call 	getOffset
	lea 	dx, how_many_read_bytes
	mov 	cx, 10
	mov 	ah, 40h
	int 	21h

	lea 	dx, command_xlat
	mov 	ah, 40h
	mov 	cx, 4
	int 	21h

	mov 	cx, 2
	lea 	dx, new_line
	mov 	ah, 40h
	int     21h

	ret
construct_XLAT endp
;-----------------------------------------------
construct_OUT_Par proc near
	mov 	ch, 4
	call 	getLastBit
	cmp 	bl, 1
	je 		print_OUT_W
	jmp  	print_OUT_Par

print_OUT_W:
	xor 	bl, bl
	mov 	cl, 3 	;gaunam reiksme i hexout
	lea 	bx, HEX_OUT1
	call 	recognize
	lea 	bx, HEX_OUT2
	call 	recognize

	mov 	bx, destFHandle
	lea 	dx, command_out
	mov 	ah, 40h
	mov 	cx, 4
	int 	21h

	lea 	dx, HEX_OUT1
	mov 	ah, 40h
	int 	21h

	lea 	dx, HEX_OUT2
	mov 	ah, 40h
	int 	21h

	lea 	dx, new_line
	mov 	ah, 40h
	int     21h

	ret

print_OUT_Par:
	xor 	bl, bl
	mov 	cl, 3 	;gaunam reiksme i hexout
	lea 	bx, HEX_OUT1
	call 	recognize

	mov 	bx, destFHandle
	call 	getOffset
	lea 	dx, how_many_read_bytes
	mov 	cx, 10
	mov 	ah, 40h
	int 	21h

	lea 	dx, command_out
	mov 	ah, 40h
	mov 	cx, 3
	int 	21h

	lea 	dx, space
	mov 	ah, 40h
	mov 	cx, 1
	int 	21h

	lea 	dx, HEX_OUT1
	mov 	ah, 40h
	int 	21h

	lea 	dx, new_line
	mov 	ah, 40h
	int     21h

	ret

print_OUT:
	mov 	bx, destFHandle
	call 	getOffset
	lea 	dx, how_many_read_bytes
	mov 	cx, 10
	mov 	ah, 40h
	int 	21h

	lea 	dx, command_out
	mov 	ah, 40h
	mov 	cx, 3
	int 	21h

	mov 	cx, 1
	lea 	dx, new_line
	mov 	ah, 40h
	int     21h

	ret

construct_OUT_Par endp
;--------------------------------------------------
construct_MOV_2 proc near
	mov 	ch, 4
	call 	getLastBit
	cmp 	bl, 1
	je 		print_MOV_2_Word
	jmp 	print_MOV_2_Byte
print_MOV_2_Word:
	mov 	[selected_W], 1
	jmp 	cont_MOV_2
print_MOV_2_Byte:
	mov 	[selected_W], 0
cont_MOV_2:
	mov 	ch, 2
	call 	recognize	;naudosim bl
	cmp 	bl, 3
	je 		onlyRegister_MOV
memory_MOV_2:
	mov 	cl, 5
	call 	recognize

print_MOV_2:
	mov 	cl, 3
	lea 	bx, HEX_OUT1
	call 	recognize
	lea 	bx, HEX_OUT2
	call 	recognize
	cmp 	[selected_W], '1' 		;jeigu zodis
	je 		two_more_bytes
	jmp 	MOV_1_to_file
two_more_bytes1:
	lea 	bx, HEX_OUT3
	call 	recognize
	lea 	bx, HEX_OUT4
	call 	recognize
	
MOV_2_to_file:
	;isvedimas i faila
	mov 	bx, destFHandle
	call 	getOffset
	lea 	dx, how_many_read_bytes
	mov 	cx, 10
	mov 	ah, 40h
	int 	21h

	lea 	dx, command_mov
	mov 	ah, 40h
	mov 	cx, 4
	int 	21h

	mov 	dx, offset selected_EA
	mov 	ah, 40h
	mov 	cl, [EA_size]
	int     21h

	lea 	dx, space
	mov 	ah, 40h
	mov 	cx, 1
	int 	21h

	cmp 	[selected_W], '0'
	je 		MOV_1_to_file_byte
MOV_2_to_file_word:
	lea 	dx, HEX_OUT3
	mov 	ah, 40h
	int 	21h
	
	lea 	dx, HEX_OUT4
	mov 	ah, 40h
	int 	21h
MOV_2_to_file_byte:
	lea 	dx, HEX_OUT1
	mov 	ah, 40h
	int 	21h
	
	lea 	dx, HEX_OUT2
	mov 	ah, 40h
	int 	21h

	lea 	dx, new_line
	mov 	ah, 40h
	int     21h

	xor 	cx, cx
	mov 	cl, 1
	mov 	ch, 4
	mov 	[selectedCommand], 0
	ret
construct_MOV_2 endp
;--------------------------------------------------
construct_RET proc near
	mov 	ch, 4
	call 	getLastBit
	cmp 	bl, 1
	je 		print_RET
print_RET_Par:
	xor 	bl, bl
	mov 	cl, 3 	;gaunam reiksme i hexout
	lea 	bx, HEX_OUT1
	call 	recognize
	lea 	bx, HEX_OUT2
	call 	recognize

	mov 	bx, destFHandle
	call 	getOffset
	lea 	dx, how_many_read_bytes
	mov 	cx, 10
	mov 	ah, 40h
	int 	21h

	lea 	dx, command_ret
	mov 	ah, 40h
	mov 	cx, 4
	int 	21h

	lea 	dx, HEX_OUT1
	mov 	ah, 40h
	int 	21h

	lea 	dx, HEX_OUT2
	mov 	ah, 40h
	int 	21h

	lea 	dx, new_line
	mov 	ah, 40h
	int     21h

	ret
print_RET:
	mov 	bx, destFHandle
	lea 	dx, command_ret
	mov 	ah, 40h
	mov 	cx, 3
	int 	21h
	ret
construct_RET endp
;-------------------------------------------------
construct_INT proc near
	mov 	ch, 4
	;mov 	cl, 2 	;atpazistam kuris INT
	;jmp 	recognize_same_byte
	call 	getLastBit
	cmp 	bl, 1
	je 		print_INT

print_INT:
	xor 	bl, bl
	mov 	cl, 3 	;gaunam reiksme i hexout
	lea 	bx, HEX_OUT1
	call 	recognize
	lea 	bx, HEX_OUT2
	call 	recognize

	mov 	bx, destFHandle
	call 	getOffset
	lea 	dx, how_many_read_bytes
	mov 	cx, 10
	mov 	ah, 40h
	int 	21h

	lea 	dx, command_int
	mov 	ah, 40h
	mov 	cx, 4
	int 	21h

	mov 	cx, 1
	lea 	dx, HEX_OUT1
	mov 	ah, 40h
	int 	21h

	lea 	dx, HEX_OUT2
	mov 	ah, 40h
	int 	21h

	lea 	dx, new_line
	mov 	ah, 40h
	int     21h

	xor 	cx, cx
	mov 	cl, 1
	mov 	[selectedCommand], 0
	ret

construct_INT endp
;-----------------------------------------------
construct_MOV_1 proc near
	mov 	[selectedCommand], 'b'
	mov 	cl, 2			;cl 1 atpazinti bloka, cl 2 atpazinti komanda (parametras), cl 3 gauti reiksme, cl 4 gauti registra
;randam W
	mov 	ch, 1 			
	call 	recognize
	jmp 	cont_MOV
onlyRegister_MOV:
	mov 	ch, 4
	mov 	cl, 4
	call 	recognize
	jmp 	cont1_MOV
cont_MOV:
;randam registra
	and 	dl, 0111b
	mov 	ch, 4
	mov 	cl, 4
	call 	recognize_same_byte
cont1_MOV:
;randam reiksmes
	mov 	cl, 3
	lea 	bx, HEX_OUT1
	call 	recognize
	lea 	bx, HEX_OUT2
	call 	recognize
	cmp 	[selected_W], '1' 		;jeigu zodis
	je 		two_more_bytes
	jmp 	MOV_1_to_file
two_more_bytes:
	lea 	bx, HEX_OUT3
	call 	recognize
	lea 	bx, HEX_OUT4
	call 	recognize
	
MOV_1_to_file:
	;isvedimas i faila
	mov 	bx, destFHandle
	call 	getOffset
	lea 	dx, how_many_read_bytes
	mov 	cx, 10
	mov 	ah, 40h
	int 	21h

	lea 	dx, command_mov
	mov 	ah, 40h
	mov 	cx, 4
	int 	21h

	mov 	dx, offset selected_register
	mov 	ah, 40h
	mov 	cx, 2
	int     21h

	lea 	dx, space
	mov 	ah, 40h
	mov 	cx, 1
	int 	21h

	cmp 	[selected_W], '0'
	je 		MOV_1_to_file_byte
MOV_1_to_file_word:
	mov 	cx, 1
	lea 	dx, HEX_OUT3
	mov 	ah, 40h
	int 	21h
	
	lea 	dx, HEX_OUT4
	mov 	ah, 40h
	int 	21h

MOV_1_to_file_byte:
	mov 	cx, 1
	lea 	dx, HEX_OUT1
	mov 	ah, 40h
	int 	21h
	
	lea 	dx, HEX_OUT2
	mov 	ah, 40h
	int 	21h

	lea 	dx, new_line
	mov 	ah, 40h
	int     21h

	xor 	cx, cx
	mov 	cl, 1
	mov 	ch, 4
	mov 	[selectedCommand], 0
	jmp 	dissasemble
construct_MOV_1 endp
;--------------------------------------------------
getEA proc near
	cmp 	bl, 000b
	je 		got_BXSI
	cmp 	bl, 001b
	je 		got_BXDI
	cmp 	bl, 010b
	je 		got_BPSI
	cmp 	bl, 011b
	je 		got_BPDI
	cmp 	bl, 100b
	je 		got_SI
	cmp 	bl, 101b
	je 		got_DI
	cmp 	bl, 110b
	je 		got_addr
	cmp 	bl, 111b
	je 		got_BX
	ret
got_BXSI:
	lea 	di, selected_EA
	lea 	si, bxsi
	call 	writeToScreenBuff
	mov 	[EA_size], 5
	ret
got_BXDI:
	lea 	di, selected_EA
	lea 	si, bxdi
	call 	writeToScreenBuff
	mov 	[EA_size], 5
	ret
got_BPSI:
	lea 	di, selected_EA
	lea 	si, bpsi
	call 	writeToScreenBuff
	mov 	[EA_size], 5
	ret
got_BPDI:
	lea 	di, selected_EA
	lea 	si, bpdi
	call 	writeToScreenBuff
	mov 	[EA_size], 5
	ret
got_SI:
	lea 	di, selected_EA
	lea 	si, si_
	call 	writeToScreenBuff
	mov 	[EA_size], 2
	ret
got_DI:
	lea 	di, selected_EA
	lea 	si, di_
	call 	writeToScreenBuff
	mov 	[EA_size], 2
	ret
got_addr:
	mov 	cl, 3
	lea 	bx, HEX_OUT1
	call 	recognize
	lea 	bx, HEX_OUT2
	call 	recognize
	lea 	bx, HEX_OUT3
	call 	recognize
	lea 	bx, HEX_OUT4
	call 	recognize
	mov 	ah, [HEX_OUT3]
	mov 	[selected_EA], ah
	mov 	ah, [HEX_OUT4]
	mov 	[selected_EA+1], ah
	mov 	ah, [HEX_OUT1]
	mov 	[selected_EA+2], ah
	mov 	ah, [HEX_OUT2]
	mov 	[selected_EA+3], ah
	mov 	[EA_size], 4
	lea 	dx, selected_EA
	mov 	ah, 09h
	int 	21h
	ret

got_BX:
	lea 	di, selected_EA
	lea 	si, bx_
	call 	writeToScreenBuff
	mov 	[EA_size], 2
	ret
	
getEA endp

;---------------------------------------------------
;i bl dedam binary registro reiksme
getRegister proc near
	cmp 	[selected_W], '1'
	je 		fullRegisters
	jmp 	halfRegisters
fullRegisters:
	cmp 	bl, 000b
	je 		writeAX
	cmp 	bl, 001b
	je 		writeCX
	cmp 	bl, 010b
	je 		writeDX
	cmp 	bl, 011b
	je 		writeBX
	cmp 	bl, 100b
	je 		writeSP
	cmp 	bl, 101b
	je 		writeBP
	cmp 	bl, 110b
	je 		writeSI
	cmp 	bl, 111b
	je 		writeDI
writeAX:
	lea 	di, selected_register
	mov 	word ptr [di], "XA"
	ret
writeCX:
	lea 	di, selected_register
	mov 	word ptr [di], "XC"
	ret
writeDX:
	lea 	di, selected_register
	mov 	word ptr [di], "XD"
	ret
writeBX:
	lea 	di, selected_register
	mov 	word ptr [di], "XB"
	ret
writeSP:
	lea 	di, selected_register
	mov 	word ptr [di], "PS"
	ret
writeBP:
	lea 	di, selected_register
	mov 	word ptr [di], "PB"
	ret
writeSI:
	lea 	di, selected_register
	mov 	word ptr [di], "IS"
	ret
writeDI:
	lea 	di, selected_register
	mov 	word ptr [di], "ID"
	ret
halfRegisters:
	cmp 	bl, 000b
	je 		writeAL
	cmp 	bl, 001b
	je 		writeCL
	cmp 	bl, 010b
	je 		writeDL
	cmp 	bl, 011b
	je 		writeBL
	cmp 	bl, 100b
	je 		writeAH
	cmp 	bl, 101b
	je 		writeCH
	cmp 	bl, 110b
	je 		writeDH
	cmp 	bl, 111b
	je 		writeBH
writeAL:
	lea 	di, selected_register
	mov 	word ptr [di], "LA"
	ret
writeCL:
	lea 	di, selected_register
	mov 	word ptr [di], "LC"
	ret
writeDL:
	lea 	di, selected_register
	mov 	word ptr [di], "LD"
	ret
writeBL:
	lea 	di, selected_register
	mov 	word ptr [di], "LB"
	ret
writeAH:
	lea 	di, selected_register
	mov 	word ptr [di], "HA"
	ret
writeCH:
	lea 	di, selected_register
	mov 	word ptr [di], "HC"
	ret
writeDH:
	lea 	di, selected_register
	mov 	word ptr [di], "HD"
	ret
writeBH:
	lea 	di, selected_register
	mov 	word ptr [di], "HB"
	ret
getRegister endp

printOffset proc near

	ret
endp

getByte proc near
	mov 	al, byte ptr [si]
	;kita karta skaitysime kita baita is buffer
	inc 	si
	;issaugom kiek baitu skaityti liko ir nuskaitom is failo kai nebelieka
	mov 	cl, [bytesLeft]
	dec 	cl
	mov 	[bytesLeft], cl
	cmp 	cl, 0
	je 		read

	ret
getByte endp

writeToScreenBuff proc near
	;si source bufferis, di dest bufferis
checkIfEnd:
	cmp		byte ptr ds:[si], '$'
	je 		endWrite
writeBuff:
	lodsb
	stosb
	jmp 	checkIfEnd
endWrite:
	ret
writeToScreenBuff endp

clearScreenBuff proc near
	lea 	si, screenBuff
	mov 	cx, 30
clearBuff:
	mov 	byte ptr [si], 0
	inc 	si
	loop 	clearBuff
	ret
clearScreenBuff endp

skip_spaces PROC near

skip_spaces_loop:
	cmp 	byte ptr ds:[si], ' '
	jne 	skip_spaces_end
	inc 	si
	jmp 	skip_spaces_loop
skip_spaces_end:
	ret
skip_spaces ENDP

read_filename PROC near
	push	ax
	call	skip_spaces
read_filename_start:
	cmp		byte ptr ds:[si], 13	; jei nera parametru
	je		read_filename_end	; tai taip, tai baigtas failo vedimas
	cmp		byte ptr ds:[si], ' '	; jei tarpas
	jne		read_filename_next	; tai praleisti visus tarpus, ir sokti prie kito parametro
read_filename_end:
	mov		al, '$'			; irasyti '$' gale
	stosb                           ; Store AL at address ES:(E)DI, di = di + 1
	pop		ax
	ret
read_filename_next:
	lodsb				; uzkrauna kita simboli
	stosb                           ; Store AL at address ES:(E)DI, di = di + 1
	jmp 	read_filename_start

read_filename ENDP

convert proc near    
	;mov 	cx, 2
charLoop:
	;dec 	cx					
	mov 	al, dl
	;shr 	dl, 4
	;and 	al, 0Fh 			;gaunam 4 bitus ;0xF

	;mov 	bx, offset HEX_OUT   ; paruosiam HEX_OUT
  	;add 	bx, 1        ; pridedam cx, kad nustatyti kelintas baitas

	cmp 	al, 0Ah			;jei skaicius
	jl 		setHex
	add 	al, 27h			;jei raide
	jl 		setHex

setHex:
	add 	al, 30h
	mov 	byte [bx-1],al  ; pridedam al reiksme prie bx esancio string'o tam tikrose vietose
	;cmp 	cx, 0
	;je 		convertDone
	;jmp 	charLoop

convertDone:
	ret
convert endp 

getOffset proc near    
	push 	ax
	push 	bx
	push 	cx
	push 	dx
	mov 	cx, 4
	mov 	dx, [total_bytes_read]	
charLoop1:
	dec 	cx			
	mov 	ax, dx
	shr 	dx, 4
	and 	ax, 0Fh 			;gaunam 4 bitus ;0xF

	mov 	bx, offset how_many_read_bytes   ; paruosiam HEX_OUT
  	add 	bx, cx        ; pridedam cx, kad nustatyti kelintas baitas

	cmp 	ax, 0Ah			;jei skaicius
	jl 		setHex1
	add 	al, 27h			;jei raide
	jl 		setHex1

setHex1:
	add 	al, 30h
	mov 	byte [bx-1],al  ; pridedam al reiksme prie bx esancio string'o tam tikrose vietose
	cmp 	cx, 0
	je 		convertDone1
	jmp 	charLoop1

convertDone1:
	pop 	dx
	pop 	cx
	pop 	bx
	pop 	ax
	ret
getOffset endp

closing:

	mov		bx, sourceFHandle	; pabaiga skaitomo failo
	mov		ah, 3eh			; uzdaryti
	int		21h

	mov 	bx, destFHandle
	mov 	ah, 3eh
	int 	21h

	mov 	ah, 4ch
	mov 	al, 0 		        ; be klaidu = 0
	int 	21h                     ; 21h -  dos pertraukimmas - http://www.computing.dcu.ie/~ray/teaching/CA296/notes/8086_bios_and_dos_interrupts.html#int21h
end start
