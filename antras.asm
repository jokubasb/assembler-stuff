.model small
.stack 100h
jumps
.data
Simboliu_kiekis = 50
File1   	db 'file1.txt$' , 0
File1Handle 	dw ?
File2		db 'file2.txt$' , 0
File2Handle 	dw ?
err_s    	db 'Source/destination failo nepavyko atidaryti skaitymui',13,10,'$'
Buffer  db Simboliu_kiekis dup (?)
Buffer2 db Simboliu_kiekis dup (?)
File1Simboliai dw 0
File2Simboliai dw 0
Nmatchingsymbols db 'labas' 
newline db 13,10,'$'

.code

start:
	;iniciavimas
	mov		ax, @data		
	mov		ds, ax	
	;pirmo failo atdr
	lea 	dx, File1
	mov 	al, 00
	mov 	ah, 3Dh
	int  	21h
	jc 		file_error
	mov 	File1Handle, ax
	;antro fail atdr
	lea 	dx, File2
	mov 	al, 00
	mov 	ah, 3Dh
	int  	21h
	jc 		file_error2
	mov 	File2Handle, ax
	skaitymas:
	;nuskaito pirmo failo 50 simboliu
	mov  AH , 3Fh
	mov	 BX , File1Handle
	mov  CX , Simboliu_kiekis
	lea  DX , Buffer
	int  21h
	mov  File1Simboliai , ax
	
	
	;nuskaito antro failo 50 simboliu
	mov  AH , 3Fh
	mov	 BX , File2Handle
	mov  CX , Simboliu_kiekis
	lea  DX , Buffer2
	int  21h
	mov File2Simboliai , ax
	;Issrienkame didesni faila
	cmp File1Simboliai , 0
	je closing
	mov cx , File1Simboliai
	mov si , 0
	ciklas:
	mov al , Buffer[si]
	cmp al , Buffer2[si]
	je testi
	
	testi:
	mov  dx ,File1Simboliai 
	cmp  dx , File2Simboliai
	je @ciklas
	jg jei_antras_mazesnis
	jmp jei_pirmas_mazesnis
	@ciklas:
	loop ciklas
	jmp isvedimas
	jmp skaitymas
	
	;mov cx , 50
	jei_pirmas_mazesnis:
	mov AH , 40h
	mov BX , 1
	inc si 
	sub	File2Simboliai , si
	mov dx , offset Buffer2
	add dx, si
	mov cx , File2Simboliai	
	int 21h
	dec cx 
	cmp cx , 0
	je closing
	lea dx, newline
	mov ah, 09h
	int 21h
	jmp jei_pirmas_mazesnis
	
	
	jei_antras_mazesnis:
	mov AH , 40h
	mov BX , 1
	inc si 
	sub	File1Simboliai , si
	mov dx , offset Buffer
	add dx, si
	mov cx , File1Simboliai
	int 21h
	dec cx
	cmp cx , 0
	je closing
	lea dx, newline
	mov ah, 09h
	int 21h
	jmp jei_antras_mazesnis
	


	isvedimas:
	mov	AH , 40h
	mov	BX , 1
	mov	CX , 5
	mov	DX , offset Nmatchingsymbols
	int 21h
	
file_error2:
	mov		bx, File1Handle	; pabaiga skaitomo failo
	mov		ah, 3eh			; uzdaryti
	int		21h
file_error:
	mov		dx, offset err_s        
	mov		ah, 09h
	int		21h
	jmp 	closing

	
closing:
	mov 	ah, 4ch
	mov 	al, 0 		        ; be klaidu = 0
	int 	21h   
	
	end start