.model small
.stack 100h

.data

apie    	db 'Programa isvedanti faila sesioliktainemis reiksmemis',13,10,'$'
err_s    	db 'Source failo nepavyko atidaryti skaitymui',13,10,'$'
filename	db 'simple.txt$'
HEX_OUT 	db '00',0,'$'
new_line 	db 	13,10,'$'
offsetNumber db 	'00000000',0,'$'

buffer  	db 15 dup (?)

sourceF   	db 12 dup (0)
sourceFHandle	dw ?

.code

start:
	mov 	dx, @data            	; perkelti data i registra ax
	mov 	ds, dx             		; nustatyti ds rodyti i data segmenta
	;mov 	si, 81h 				;parametrai prasideda nuo 81
	mov 	ah, 09h
	lea 	dx, apie
	int 	21h

source_from_file:
	lea		dx, filename	; failo pavadinimas
	mov		ah, 3dh                	; atidaro faila - komandos kodas
	mov		al, 0                  	; 0 - reading, 1-writing, 2-abu
	int		21h			; INT 21h / AH= 3Dh - open existing file
	;jc		ending		; CF set on error AX = error code.
	mov		sourceFHandle, ax	; issaugojam filehandle
	xor 	ax, ax
	push 	ax


read:
	mov		bx, sourceFHandle
	lea		dx, buffer       ; address of buffer in dx
	mov		cx, 15         		; kiek baitu nuskaitysim, po to sita naudoja LODSB
	mov		ah, 3fh         	; function 3Fh - read from file
	int		21h

	mov 	si, offset buffer 	;si po to keliauja i LODSB
	;jc 		ending

	pop 	dx 				;skaiciuohame kiek is viso baitu nuskaiteme
	add 	dx, ax			;naudojamas offsetui
	add 	dx, 1
	push 	dx

	mov		cx, ax          	; bytes actually read
	push 	cx 				;pushinam kad pasiektu rawtext
	push 	cx				;pushinam kad pasiektu getoffset
	cmp		ax, 0			; jei nenuskaite
	jne 	printOffset
	jmp		ending			; tai ne pabaiga

printOffset:
	lea 	dx,	offsetNumber
	mov 	ah, 09h
	int 	21h

	pop 	ax 				;nuskaitytu bitu countas
	pop 	bx
	pop 	dx				;visu nuskaitytu bitu countas
	push 	dx
	push 	bx
	push 	ax
	call 	getOffset
	pop 	cx
line:
	LODSB 					
	mov 	dl, al
	push 	cx
	call 	convert
	pop 	cx

	lea 	dx, HEX_OUT 	;hex isvedimas
	mov 	ah, 09h
	int 	21h
	
	loop 	line 			;cx-- loopas baigiasi kai cx = 0

	pop 	cx
	call 	printRawText

	lea 	dx, new_line 	;nauja eilute po 15 baitu
	mov 	ah, 09h
	int 	21h
	inc 	ah
	jmp 	read

printRawText proc near
	mov 	si, offset buffer

	mov 	dl, '|'
	mov 	ah, 2
	int 	21h
printChar:
	LODSB 
	cmp 	al, 1fh
	jle 	notSymbol
	cmp 	al, 7dh
	jge 	notSymbol
	mov 	dl, al
printOne:
	mov 	ah, 2
	int 	21h
	loop 	printChar

	mov 	dl, '|'
	mov 	ah, 2
	int 	21h

	ret
notSymbol:
	mov 	dl, '.'
	jmp 	printOne
printRawText endp

;------------galime sito nebeliesti!
convert proc near    
	mov 	cx, 2
charLoop:
	dec 	cx					
	mov 	al, dl
	shr 	dl, 4
	and 	al, 0Fh 			;gaunam 4 bitus ;0xF

	mov 	bx, offset HEX_OUT   ; paruosiam HEX_OUT
  	add 	bx, cx        ; pridedam cx, kad nustatyti kelintas baitas

	cmp 	al, 0Ah			;jei skaicius
	jl 		setHex
	add 	al, 27h			;jei raide
	jl 		setHex

setHex:
	add 	al, 30h
	mov 	byte [bx-1],al  ; pridedam al reiksme prie bx esancio string'o tam tikrose vietose
	cmp 	cx, 0
	je 		convertDone
	jmp 	charLoop

convertDone:
	ret
convert endp 

getOffset proc near    
	mov 	cx, 8
charLoop1:
	dec 	cx					
	mov 	ax, dx
	shr 	dx, 4
	and 	ax, 0Fh 			;gaunam 4 bitus ;0xF

	mov 	bx, offset offsetNumber   ; paruosiam HEX_OUT
  	add 	bx, cx        ; pridedam cx, kad nustatyti kelintas baitas

	cmp 	ax, 0Ah			;jei skaicius
	jl 		setHex1
	add 	al, 27h			;jei raide
	jl 		setHex1

setHex1:
	add 	al, 30h
	mov 	byte [bx-1],al  ; pridedam al reiksme prie bx esancio string'o tam tikrose vietose
	cmp 	cx, 0
	je 		convertDone1
	jmp 	charLoop1

convertDone1:
	ret
getOffset endp 

ending:
	mov	bx, sourceFHandle	; pabaiga skaitomo failo
	mov	ah, 3eh			; uzdaryti
	int	21h

	mov 	ah, 4ch
	mov 	al, 0 		        ; be klaidu = 0
	int 	21h                     ; 21h -  dos pertraukimmas - http://www.computing.dcu.ie/~ray/teaching/CA296/notes/8086_bios_and_dos_interrupts.html#int21h
end start
